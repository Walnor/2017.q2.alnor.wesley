#pragma once
#include<iostream>
#include<ExportHeader.h>
#include"Vec2.h"

	namespace Engine
{
	// Mat3 is an immutable vector class, except for assignment
	class ENGINE_SHARED Mat3
	{
	public:
		Mat3();
		Mat3(float id);
		Mat3(Vec2 ID);
		Mat3(float a, float b, float d, float e);
		Mat3(float a, float b, float c, float d, float e, float f, float g, float h, float i);
		~Mat3();

	public:
		static Mat3 Rotate(float degrees);
		static Mat3 Scale(float scale);
		static Mat3 Scale(Vec2 scale);
		static Mat3 Scale(float scaleX, float scaleY);
		static Mat3 ScaleX(float scaleX);
		static Mat3 ScaleY(float scaleY);
		static Mat3 RotAndScale(float degrees, float scale);
		static Mat3 RotAndScale(float degrees, Vec2 scale);
		static Mat3 Translation(float x, float y);
		static Mat3 Translation(Vec2 t);

	public:
		Mat3 operator*(Mat3 right) const;
		Vec2 operator*(Vec2 right) const;
		Mat3 Mult(Vec2 Vec) const;

		bool operator==(Mat3 right) const;

		float* Pos() { return &m_a; }

		std::ostream & Output(std::ostream & os) const;
	public:
		float m_a, m_b, m_c;
		float m_d, m_e, m_f;
		float m_g, m_h, m_i;
	};

	inline std::ostream& operator<< (std::ostream& os, const Mat3& v)
	{
		v.Output(os);

		return os;
	}

}