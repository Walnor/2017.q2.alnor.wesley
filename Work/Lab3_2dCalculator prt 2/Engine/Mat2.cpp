#include "Mat2.h"

namespace Engine
{

	Mat2::Mat2() : m_a{ 1.0f }, m_b{ 0.0f }, m_c{ 0.0f }, m_d{ 1.0f }
	{
	}

	Mat2::Mat2(float identity) : m_a{ identity }, m_b{ 0.0f }, m_c{ 0.0f }, m_d{ identity }
	{
	}

	Mat2::Mat2(float a, float b, float c, float d)
	{
		m_a = a;
		m_b = b;
		m_c = c;
		m_d = d;
	}

	Mat2::Mat2(Vec2 identity) : m_a{ identity.x }, m_b{ 0.0f }, m_c{ 0.0f }, m_d{ identity.y }
	{
	}

	Mat2::Mat2(Vec2 R1, Vec2 R2) : m_a{ R1.x }, m_b{ R1.y }, m_c{ R2.x }, m_d{ R2.y }
	{
	}

	Mat2 Mat2::Rotate(float degrees)
	{
		return Mat2(cos(degrees), -sin(degrees), sin(degrees), cos(degrees));
	}

	Mat2 Mat2::Scale(float scale)
	{
		return Mat2(scale, 0, 0, scale);
	}

	Mat2 Mat2::Scale(Vec2 scale)
	{
		return Mat2(scale.x, 0, 0, scale.y);
	}

	Mat2 Mat2::Scale(float scaleX, float scaleY)
	{
		return Mat2(scaleX, 0, 0, scaleY);
	}

	Mat2 Mat2::ScaleX(float scaleX)
	{
		return Mat2(scaleX, 0, 0, 1);
	}

	Mat2 Mat2::ScaleY(float scaleY)
	{
		return Mat2(1, 0, 0, scaleY);
	}

	Mat2 Mat2::RotAndScale(float degrees, float scale)
	{
		return Mat2(scale, 0, 0, scale) * Mat2(cos(degrees), -sin(degrees), sin(degrees), cos(degrees));
	}

	Mat2 Mat2::RotAndScale(float degrees, Vec2 scale)
	{
		return Mat2(scale.x, 0, 0, scale.y) * Mat2(cos(degrees), -sin(degrees), sin(degrees), cos(degrees));
	}

	Mat2 Mat2::operator*(Mat2 right) const
	{
		float a = (m_a * right.m_a) + (m_b * right.m_c);
		float b = (m_a * right.m_b) + (m_b * right.m_d);

		float c = (m_c * right.m_a) + (m_d * right.m_c);
		float d = (m_c * right.m_b) + (m_d * right.m_d);

		return Mat2(a, b, c, d);
	}

	Vec2 Mat2::operator*(Vec2 right) const
	{
		float a = (m_a * right.x) + (m_b * right.y);
		float b = (m_c * right.x) + (m_d * right.y);
		return Vec2(a, b);
	}

	Mat2 Mat2::operator*(float right) const
	{
		return Mat2(m_a * right, m_b * right, m_c * right, m_d * right);
	}

	Mat2 Mat2::operator+(Mat2 right) const
	{
		return Mat2(m_a + right.m_a, m_b + right.m_b, m_c + right.m_c, m_d + right.m_d);
	}

	Mat2 Mat2::operator-(Mat2 right) const
	{
		return Mat2(m_a - right.m_a, m_b - right.m_b, m_c - right.m_c, m_d - right.m_d);
	}

	Mat2 Mat2::operator-() const
	{
		return Mat2(-m_a, -m_b, -m_c, -m_d);
	}

	bool Mat2::operator==(Mat2 right) const
	{
		if (m_a != right.m_a)
			return false;
		if (m_b != right.m_b)
			return false;
		if (m_c != right.m_c)
			return false;
		if (m_d != right.m_d)
			return false;
		return true;
	}

	std::ostream & Mat2::Output(std::ostream & os) const
	{
		os << "[ { " << m_a << "  " << m_b << " } { " << m_c << "  " << m_d << " } ]";
		return os;
	}

	Mat2 operator*(float left, Mat2 right)
	{
		return right * left;
	}
}