#include "AffineTransformTab.h"
#include"RenderUI.h"
#include"Mat3.h"

using namespace Engine;
namespace
{
	Mat3 matrix;

	Vec2 VecA;
	Vec2 VecB;
	Vec2 VecC;
	Vec2 VecD;
	Vec2 VecE;

	Vec2 ResultA;
	Vec2 ResultB;
	Vec2 ResultC;
	Vec2 ResultD;
	Vec2 ResultE;

	float pos[15];

	//This callback appears to want a vec3 class... I don't have a vec3 class yet,
	//for now I will just calculate it using the Vec2 class and ignore the 3rd float
	//of the vectors that the data is trying to give me, I will just return them without
	//doing anything to them.
	void TransformCallback(const AffineTransformationData& data)
	{
		matrix = Mat3(*data.data, *(data.data + 1), *(data.data + 2),
			*(data.data + 3), *(data.data + 4), *(data.data + 5),
			*(data.data + 6), *(data.data + 7), *(data.data + 8));
		VecA = Vec2(*(data.data + 9), *(data.data + 10));
		VecB = Vec2(*(data.data + 12), *(data.data + 13));
		VecC = Vec2(*(data.data + 15), *(data.data + 16));
		VecD = Vec2(*(data.data + 18), *(data.data + 19));
		VecE = Vec2(*(data.data + 21), *(data.data + 22));

		ResultA = matrix * VecA;
		ResultB = matrix * VecB;
		ResultC = matrix * VecC;
		ResultD = matrix * VecD;
		ResultE = matrix * VecE;

		pos[0] = ResultA.x;
		pos[1] = ResultA.y;
		pos[2] = *(data.data + 11);
		pos[3] = ResultB.x;
		pos[4] = ResultB.y;
		pos[5] = *(data.data + 14);
		pos[6] = ResultC.x;
		pos[7] = ResultC.y;
		pos[8] = *(data.data + 17);
		pos[9] = ResultD.x;
		pos[10] = ResultD.y;
		pos[11] = *(data.data + 20);
		pos[12] = ResultE.x;
		pos[13] = ResultE.y;
		pos[14] = *(data.data + 23);
	}
}


AffineTransformTab::AffineTransformTab(RenderUI* pRenderUI) : m_isInitialized(false)
{
	m_isInitialized = Initialize(pRenderUI);
}


AffineTransformTab::~AffineTransformTab()
{
}

bool AffineTransformTab::Initialize(RenderUI * pRenderUI)
{
	pRenderUI->setAffineTransformationData(&pos[0], TransformCallback);
	return false;
}

bool AffineTransformTab::Shutdown()
{
	return false;
}
