#pragma once

class RenderUI;
class AdditionTab
{
public:
	AdditionTab(RenderUI* pRenderUI);
	~AdditionTab();


	bool isInitialized() const { return m_isInitialized; }

private:
	bool m_isInitialized = false;

	bool Initialize(RenderUI* pRenderUI);
	bool Shutdown();
};

