#include<iostream>
#include"RenderUI.h"
#include"TabManager.h"
#include"AdditionTab.h"
#include"Engine.h"
#include"Mat2.h"

using namespace Engine;

int main(int argc, char** argv)
{
	std::cout << "Hello World!\n";

	if (!Engine::Initialized()) return -2;

	RenderUI render;
	TabManager tabs(&render);

	Mat2 a(2.0f);
	Mat2 b(1.0f, 0, 0, 5.0f);
	Mat2 c = a + b;
	Mat2 d = Mat2::Rotate(PI/4);


	//std::cout << c << std::endl;
	std::cout << d << std::endl;

	if (!render.initialize(argc, argv))	return -1;

	render.run();
	render.shutdown();

	Engine::Shutdown();

	return 0;
}