#include "MatrixMultTab.h"
#include"RenderUI.h"
#include"Mat2.h"

using namespace Engine;
namespace
{
	Mat2 matrix(0);
	Vec2 Vector(0,0);
	Vec2 result(0,0);
	void MatCallBack(const LinearTransformationData& data)
	{
		matrix = Mat2(data.m00, data.m01, data.m10, data.m11);
		Vector = Vec2(data.v0, data.v1);

		result = matrix * Vector;
	}
}


MatrixMultTab::MatrixMultTab(RenderUI* pRenderUI) : m_isInitialized(false)
{
	m_isInitialized = Initialize(pRenderUI);
}


MatrixMultTab::~MatrixMultTab()
{
}

bool MatrixMultTab::Initialize(RenderUI * pRenderUI)
{
	pRenderUI->setLinearTransformationData(result.Pos() , MatCallBack);
	return false;
}

bool MatrixMultTab::Shutdown()
{
	return false;
}

