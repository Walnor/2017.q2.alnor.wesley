#pragma once
#include"GameObject.h"
#include "Waypoint.h"
#include "Turret.h"

class PatrolShip : public GameObject
{
public:
	PatrolShip();
	PatrolShip(float x, float y);
	~PatrolShip();

	bool Update(float dt);
	void Draw(Core::Graphics& g);

	void GiveWaypoint(Waypoint point);

	void GiveTarget(GameObject * t) { m_turretTarget = t; }

	float & giveSpeed() { return m_Speed; }
	Vec2 & giveDirection() { return m_Direction; }
private:
	void SetShape();
	int m_waypoints{ 0 };

	Turret m_turret;
	GameObject * m_turretTarget{ nullptr };

	Waypoint * m_target{ nullptr };
	Waypoint m_points[4];
};

