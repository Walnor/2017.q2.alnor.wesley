#include "fpsUI.h"
#include<iostream>
#include"Shape.h"

fpsUI::fpsUI()
{
}

fpsUI::~fpsUI()
{
}

bool fpsUI::Update(float dt)
{
	static float frames = 0;
	static int frameCount = 0;
	frames += dt;
	frameCount++;

	if (frames > 1)
	{//This easier to read and is probably a bit more accurate.
		m_framesPerSec = (1.0f / frames) * frameCount;
		m_MilisecPerFrame = 1000.0f / m_framesPerSec;
		frameCount = 0;
		frames = 0;
	}

	//m_MilisecPerFrame = dt * 1000;
	//m_framesPerSec = 1000 / m_MilisecPerFrame;
	return false;
}

void fpsUI::Draw(Core::Graphics & g)
{
	g.SetColor(RGB(255, 255, 255));
	char fps[12];
	snprintf(fps, sizeof fps, "FPS: %f", m_framesPerSec);

	char mpf[12];
	snprintf(mpf, sizeof mpf, "MPF: %f", m_MilisecPerFrame);

	g.DrawString(1000, 20, fps);
	g.DrawString(1000, 50, mpf);

	///Never fully implimented
	//char ModeDistription[45];
	//snprintf(ModeDistription, sizeof ModeDistription, "1 wrap : 2 bounce    Current Mode : %s", "wrap");

	//g.DrawString(20, 40, ModeDistription);

	if (m_ObjManagerPnt != nullptr)
	{
		char DFW[28];
		snprintf(DFW, sizeof DFW, "Distance from Wall: %f", m_ObjManagerPnt->DistanceFromClosestWall());

		g.DrawString(100, 20, DFW);
	}
}
