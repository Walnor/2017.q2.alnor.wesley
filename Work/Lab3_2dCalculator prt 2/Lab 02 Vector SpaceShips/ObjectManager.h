#pragma once
#include"PatrolShip.h"
#include"Wall.h"
#include"Turret.h"
class ObjectManager
{
	const int m_MaxObjects = 100;
public:
	ObjectManager();
	~ObjectManager();


	bool Update(float dt);
	void Draw(Core::Graphics& g);

	void AddObject(GameObject toAdd);
	void AddPlayer(Ship *toAdd);
	void AddWall(Wall toAdd);
	void AddNPC(PatrolShip toAdd);

	void AddPlayerTurret();

	void Wrap(GameObject& object);
	void Bounce(GameObject& object);

	void giveDimentions(float width, float height) { m_width = width; m_height = height; }

	float DistanceFromClosestWall();

	Wall & getWall(int index) { return m_walls[index]; }
	Ship * playerShip() { return &m_player; }
	PatrolShip * GetNPC() { return &m_npc; }
private:
	bool Initialize();
	bool Shutdown();

private:
	bool m_isInitialized{ false };

	float m_width, m_height;

	int m_NumOfObjects{ 0 };
	int m_NumOfWalls{ 0 };

	std::vector<Wall> m_walls;

	Ship m_player;
	Turret m_playerTurret;

	PatrolShip m_npc;

	std::vector<GameObject> m_Objects;
};

