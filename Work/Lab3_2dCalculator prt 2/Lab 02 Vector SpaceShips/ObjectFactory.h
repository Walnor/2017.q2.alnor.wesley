#pragma once
#include "GameObject.h"
#include"ObjectManager.h"

static class ObjectFactory
{
public:
	static GameObject MakeRandomObject();
	static void AddWalls(ObjectManager * target, Vec2 toAdd[], int ArraySize);
private:
};

