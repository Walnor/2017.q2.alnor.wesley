#pragma once
#include"Shape.h"

using namespace Engine;
class GameObject
{
public:
	GameObject(float x = 0, float y = 0);
	~GameObject();

	bool virtual Update(float dt);
	void virtual Draw(Core::Graphics& g);

	void setVolocity(Vec2 newVol) { m_Volocity = newVol; }
	void SetPosition(Vec2 newPos) { m_Position = newPos; }

	Vec2 GetPosition() const { return m_Position; }
	Vec2 GetVolocity() const { return m_Volocity; }

	void giveColor(int r, int g, int b) { m_theShape.giveColor(r, g, b); }

	float getSpeed() const { return m_Speed; }
	Vec2 getDirection() const { return m_Direction; }

	void setSpeed(float s) { m_Speed = s; }
	void setDirection(Vec2 dir) { m_Direction = dir; }

protected:
	Vec2 m_Position{ 0.0f , 0.0f };
	Vec2 m_Volocity{ 10.0f,10.0f };

	Shape m_theShape;

	float m_Speed = 20.0f;
	Vec2 m_Direction{ 0.5f, 0.5f };

};