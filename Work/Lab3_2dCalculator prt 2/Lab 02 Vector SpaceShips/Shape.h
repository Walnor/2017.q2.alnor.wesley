#pragma once
#include"Core.h"
#include"Vec2.h"
#include<vector>

typedef unsigned long  RGB;
typedef unsigned char  BYTE;
typedef unsigned short WORD;
#define RGB(r,g,b) ((COLORREF)(((BYTE)(r)|((WORD)((BYTE)(g))<<8))|(((DWORD)(BYTE)(b))<<16)))

using namespace Engine;
class Shape
{
public:
	Shape();
	Shape(Vec2 theShape[], int arraySize);
	~Shape();
	void giveColor(int r, int g, int b);
	
	void AddPoint(Vec2 point);
	RGB getColor() const { return m_color; }
	void setScale(float s) { m_scale = s; }
	void setOffset(Vec2 offset) { m_Offset = offset; }
	void Draw(Core::Graphics& g);
private:
	RGB m_color{ RGB(255,255,255) };
	Vec2 m_Offset{ 0,0 };
	int m_points{0};
	float m_scale{ 1 };
	std::vector<Vec2> m_Shape;
};

