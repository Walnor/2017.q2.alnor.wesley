#pragma once
#include "GameObject.h"
class Ship :
	public GameObject
{
public:
	Ship();
	Ship(float x, float y);
	~Ship();

	bool Update(float dt);
	void Draw(Core::Graphics& g);

	float & giveSpeed() { return m_Speed; }
	Vec2 & giveDirection() { return m_Direction; }
private:
	void SetShape();
};

