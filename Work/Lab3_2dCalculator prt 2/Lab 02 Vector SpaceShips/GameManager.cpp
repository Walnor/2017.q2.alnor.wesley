#include "GameManager.h"
#include "ObjectManager.h"
#include"PatrolShip.h"
#include"Wall.h"
#include"fpsUI.h"

namespace
{
	ObjectManager objects;
	Ship a(500, 500);
	void DefaultStartState();

	fpsUI fraps;

	bool Update(float dt)
	{
		static int num = 0;
		
		if (num > 20)
		{
			objects.Update(dt);
			fraps.Update(dt);
		}

		if (num == 0)
			DefaultStartState();

		num++;
		return false;
	}

	void Draw(Core::Graphics & g)
	{
		objects.Draw(g);
		fraps.Draw(g);
	}

	void DefaultStartState()
	{
		a.giveColor(rand() % 255, rand() % 255, rand() % 255);
		a.setVolocity(Vec2(0, 0));
		objects.AddPlayer(&a);

		Vec2 walls[] = { Vec2(200, 50) , Vec2(500, 200), Vec2(300, 20), Vec2(1100, 100), Vec2(1000, 720),
			Vec2(400, 600), Vec2(100, 500), Vec2(5, 300), Vec2(5, 120), Vec2(200, 50) };

		ObjectFactory::AddWalls(&objects, walls, 10);

		Waypoint wa(200, 500);
		Waypoint wb(400, 100);
		Waypoint wc(900, 200);
		Waypoint wd(900, 650);

		PatrolShip pa(720, 400);

		objects.AddNPC(pa);

		objects.GetNPC();

		objects.GetNPC()->GiveWaypoint(wa);
		objects.GetNPC()->GiveWaypoint(wb);
		objects.GetNPC()->GiveWaypoint(wc);
		objects.GetNPC()->GiveWaypoint(wd);

		objects.AddObject(wa);
		objects.AddObject(wb);
		objects.AddObject(wc);
		objects.AddObject(wd);

		objects.AddPlayerTurret();

		objects.GetNPC()->GiveTarget(objects.playerShip());

		fraps.giveObjMan(&objects);
	}
}

GameManager::GameManager(int width, int height)
	: m_screenWidth(width),
m_screenHeight(height)
{
	Initialize();
}

GameManager::~GameManager()
{
}

bool GameManager::RunGame()
{
	Core::GameLoop();
	Core::Shutdown();
	return true;
}

bool GameManager::Initialize()
{
	Core::Init("Not So Endless Space 0.01", m_screenWidth, m_screenHeight, 2500);
	Core::RegisterUpdateFn(Update);
	Core::RegisterDrawFn(Draw);
	m_isInitialized = true;

	objects.giveDimentions(static_cast<float>(m_screenWidth), static_cast<float>(m_screenHeight));

	return true;
}

bool GameManager::Shutdown()
{
	return true;
}
