#pragma once
#include "GameObject.h"
class Waypoint :
	public GameObject
{
public:
	Waypoint();
	Waypoint(float x, float y);
	~Waypoint();

	void Draw(Core::Graphics& g);
	Vec2 GetPosition() { return m_Position; }
private:
	void SetShape();
};

