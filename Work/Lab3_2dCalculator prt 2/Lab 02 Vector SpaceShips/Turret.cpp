#include "Turret.h"
#include"Shape.h"
#include<iostream>


Turret::Turret()
{
}


Turret::~Turret()
{
}

void Turret::Attach(GameObject * target)
{
	m_base = target;
}

bool Turret::Update(float dt)
{
	if (m_base != nullptr)
	{
		m_PointA = m_base->GetPosition();
	}

	m_PointB = m_PointA + (m_Direction * 15);
	return false;
}

void Turret::Draw(Core::Graphics & g)
{
	g.SetColor(RGB(255, 255, 100));
	g.DrawLine(m_PointA.x, m_PointA.y, m_PointB.x, m_PointB.y);
}
