#pragma once

class RenderUI;
class PerpendicularTab
{
public:
	PerpendicularTab(RenderUI* pRenderUI);
	~PerpendicularTab();


	bool isInitialized() const { return m_isInitialized; }

private:
	bool m_isInitialized = false;

	bool Initialize(RenderUI* pRenderUI);
	bool Shutdown();
};

