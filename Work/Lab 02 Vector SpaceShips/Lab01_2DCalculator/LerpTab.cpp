#include "LerpTab.h"
#include"RenderUI.h"
#include"Vec2.h"

using namespace Engine;
namespace
{
	Vec2 ZeroVector(0.0f, 0.0f);
	Vec2 aVector;
	Vec2 bVector;
	Vec2 bMinusAVector;
	Vec2 aVectorLerpPortion;
	Vec2 bVectorLerpPortion;
	Vec2 lerpResultVector;
	void LerpCallback(const LerpData& data)
	{
		aVector = Vec2(data.a_i, data.a_j);
		bVector = Vec2(data.b_i, data.b_j);
		bMinusAVector = bVector - aVector;
		lerpResultVector = aVector.Lerp(bVector, data.beta);
		aVectorLerpPortion = ZeroVector.Lerp(aVector, 1.0f-data.beta);
		bVectorLerpPortion = Lerp(ZeroVector, bVector, data.beta);
	}
}


LerpTab::LerpTab(RenderUI* pRenderUI) : m_isInitialized(false)
{
	m_isInitialized = Initialize(pRenderUI);
}


LerpTab::~LerpTab()
{
}

bool LerpTab::Initialize(RenderUI * pRenderUI)
{
	pRenderUI->setLerpData(aVector.Pos(), bVector.Pos(), bMinusAVector.Pos(), aVectorLerpPortion.Pos(),
		bVectorLerpPortion.Pos(), lerpResultVector.Pos(), LerpCallback);
	return false;
}

bool LerpTab::Shutdown()
{
	return false;
}
