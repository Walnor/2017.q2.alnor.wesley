#pragma once
class RenderUI;
class LerpTab
{
public:
	LerpTab(RenderUI* pRenderUI);
	~LerpTab();


	bool isInitialized() const { return m_isInitialized; }

private:
	bool m_isInitialized = false;

	bool Initialize(RenderUI* pRenderUI);
	bool Shutdown();
};
