#include "PlayerInput.h"

namespace
{
	using Core::Input;

	Vec2 nullDirection(0, 0);
	float nullspeed = 0;

	float maxSpeed = 400;
	float Acceleration = 1200;
}

PlayerInput::PlayerInput() : m_Direction{nullDirection}, m_Speed{nullspeed}
{
}

PlayerInput::PlayerInput(Ship * playerShip) : m_Direction{playerShip->getDirection()}, m_Speed{ playerShip->giveSpeed() }
{
	m_PlayerShip = playerShip;
}

PlayerInput::~PlayerInput()
{
}

void PlayerInput::Wall(Vec2 Dir)
{
	m_Direction = Dir.Normalize();
	m_Speed *= m_Direction * (m_PlayerShip->GetVolocity().Normalize());

	if (m_Speed > maxSpeed)
	{
		m_Speed = maxSpeed;
	}

	m_PlayerShip->setSpeed(m_Speed);
	m_PlayerShip->setDirection(m_Direction);
}

void PlayerInput::MoveUp(float dt)
{
	Vec2 newVolocity = (m_PlayerShip->GetVolocity() + (Vec2(0, -Acceleration))*dt);
	m_Speed = newVolocity.Length();
	m_Direction = newVolocity.Normalize();

	if (m_Speed > maxSpeed)
	{
		m_Speed = maxSpeed;
	}

	m_PlayerShip->setVolocity(m_Speed * m_Direction);
}

void PlayerInput::MoveDown(float dt)
{
	Vec2 newVolocity = (m_PlayerShip->GetVolocity() + (Vec2(0, Acceleration))*dt);
	m_Speed = newVolocity.Length();
	m_Direction = newVolocity.Normalize();

	if (m_Speed > maxSpeed)
	{
		m_Speed = maxSpeed;
	}

	m_PlayerShip->setVolocity(m_Speed * m_Direction);
}

void PlayerInput::MoveLeft(float dt)
{
	Vec2 newVolocity = (m_PlayerShip->GetVolocity() + (Vec2(-Acceleration, 0))*dt);
	m_Speed = newVolocity.Length();
	m_Direction = newVolocity.Normalize();

	if (m_Speed > maxSpeed)
	{
		m_Speed = maxSpeed;
	}

	m_PlayerShip->setVolocity(m_Speed * m_Direction);
}

void PlayerInput::MoveRight(float dt)
{

	Vec2 newVolocity = (m_PlayerShip->GetVolocity() + (Vec2(Acceleration, 0))*dt);
	m_Speed = newVolocity.Length();
	m_Direction = newVolocity.Normalize();

	if (m_Speed > maxSpeed)
	{
		m_Speed = maxSpeed;
	}

	m_PlayerShip->setVolocity(m_Speed * m_Direction);

}

void PlayerInput::friction(float dt)
{
	m_Speed *= 0.995f;
}

void PlayerInput::Update(float dt)
{
	if (m_PlayerShip != nullptr)
	{	
		friction(dt);

		if (Input::IsPressed(VK_UP) || Input::IsPressed(static_cast<int>('W')))
			MoveUp(dt);
		if (Input::IsPressed(VK_DOWN) || Input::IsPressed(static_cast<int>('S')))
			MoveDown(dt);
		if (Input::IsPressed(VK_LEFT) || Input::IsPressed(static_cast<int>('A')))
			MoveLeft(dt);
		if (Input::IsPressed(VK_RIGHT) || Input::IsPressed(static_cast<int>('D')))
			MoveRight(dt);

		m_PlayerShip->setSpeed(m_Speed);
		m_PlayerShip->setDirection(m_Direction);
	}

	if (m_turret != nullptr)
	{
		if(Input::IsPressed(RI_MOUSE_BUTTON_1_DOWN))
		m_turret->setDirection(-(m_PlayerShip->GetPosition() - Vec2(Input::GetMouseX(), Input::GetMouseY())).Normalize());
	}

}

void PlayerInput::Draw(Core::Graphics & g)
{
	if(m_PlayerShip != nullptr)
	m_PlayerShip->Draw(g);
}
