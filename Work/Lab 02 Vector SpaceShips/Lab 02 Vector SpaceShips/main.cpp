#include <iostream>
#include"GameManager.h"

int main()
{
	GameManager game(1280, 720);

	if (!game.IsInitialized()) return -1;
	game.RunGame();

	return 0;
}