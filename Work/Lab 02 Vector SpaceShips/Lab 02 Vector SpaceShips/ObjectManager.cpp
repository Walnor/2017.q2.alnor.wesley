#include "ObjectManager.h"

namespace
{
	PlayerInput player;
}

ObjectManager::ObjectManager()
{
}

ObjectManager::~ObjectManager()
{
}

bool ObjectManager::Update(float dt)
{
	for (int j = 0; j < m_NumOfObjects; j++)
	{
		m_Objects[j].Update(dt);
		Bounce(m_Objects[j]);
	}

	player.Update(dt);
	bool multipleWalls = false;

	for (int j = 0; j < m_NumOfWalls; j++)
	{
		if (m_walls[j].Collision(m_player, player, multipleWalls)) 
		{
			multipleWalls = true;
		}
	}

	m_player.Update(dt);
	m_npc.Update(dt);
	m_playerTurret.Update(dt);

	Wrap(m_player);
	return false;
}

void ObjectManager::Draw(Core::Graphics & g)
{
	for (int j = 0; j < m_NumOfObjects; j++)
	{
		m_Objects[j].Draw(g);
	}
	m_player.Draw(g);
	m_playerTurret.Draw(g);
	m_npc.Draw(g);
	for (int j = 0; j < m_NumOfWalls; j++)
	{
		m_walls[j].Draw(g);
	}
}

void ObjectManager::AddObject(GameObject toAdd)
{
	m_Objects.push_back(toAdd);
	m_NumOfObjects++;
}

void ObjectManager::AddPlayer(Ship *toAdd)
{
	m_player = *toAdd;
	player.GiveShip(&m_player);
}

void ObjectManager::AddWall(Wall toAdd)
{
	m_walls.push_back(toAdd);
	m_NumOfWalls++;
}

void ObjectManager::AddNPC(PatrolShip toAdd)
{
	m_npc = toAdd;
}

void ObjectManager::AddPlayerTurret()
{
	m_playerTurret.Attach(&m_player);
	player.GiveTurret(&m_playerTurret);
}

void ObjectManager::Wrap(GameObject& object)
{
	if (object.GetPosition().x > m_width)
	{
		object.SetPosition(Vec2(0, object.GetPosition().y));
	}

	if (object.GetPosition().x < 0)
	{
		object.SetPosition(Vec2(m_width, object.GetPosition().y));
	}

	if (object.GetPosition().y > m_height)
	{
		object.SetPosition(Vec2(object.GetPosition().x, 0));
	}

	if (object.GetPosition().y < 0)
	{
		object.SetPosition(Vec2(object.GetPosition().x, m_height));
	}
}

void ObjectManager::Bounce(GameObject & object)
{
	if ((object.GetPosition().x > m_width) && (object.GetVolocity().x > 0))
	{
		object.setVolocity(Vec2(-object.GetVolocity().x, object.GetVolocity().y));
	}

	if ((object.GetPosition().x) < 0 && (object.GetVolocity().x < 0))
	{
		object.setVolocity(Vec2(-object.GetVolocity().x, object.GetVolocity().y));
	}

	if ((object.GetPosition().y > m_height) && (object.GetVolocity().y > 0))
	{
		object.setVolocity(Vec2(object.GetVolocity().x, -object.GetVolocity().y));
	}

	if ((object.GetPosition().y < 0) && (object.GetVolocity().y < 0))
	{
		object.setVolocity(Vec2(object.GetVolocity().x, -object.GetVolocity().y));
	}
}

float ObjectManager::DistanceFromClosestWall()
{
	float toReturn = 10000;
	for (int j = 0; j < m_NumOfWalls; j++)
	{
		float check = m_walls[j].DistanceAway(m_player);

		if (check < toReturn) toReturn = check;
	}

	return toReturn;
}

bool ObjectManager::Initialize()
{
	m_isInitialized = true;
	return true;
}

bool ObjectManager::Shutdown()
{
	return true;
}