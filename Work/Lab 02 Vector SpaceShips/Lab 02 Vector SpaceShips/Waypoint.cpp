#include "Waypoint.h"


Waypoint::Waypoint()
{
	m_Position = Vec2(0, 0);
	m_Volocity = Vec2(0, 0);
	m_Speed = 0;
	SetShape();
	m_theShape.setOffset(m_Position);
}

Waypoint::Waypoint(float x, float y)
{
	m_Position = Vec2(x, y);
	m_Volocity = Vec2(0,0);
	m_Speed = 0;
	SetShape();
	m_theShape.setOffset(m_Position);
}

Waypoint::~Waypoint()
{
}

void Waypoint::Draw(Core::Graphics & g)
{
	m_theShape.Draw(g);
}

void Waypoint::SetShape()
{
	m_theShape.AddPoint(Vec2(-2, -2));
	m_theShape.AddPoint(Vec2(2, -2));
	m_theShape.AddPoint(Vec2(2, 2));
	m_theShape.AddPoint(Vec2(-2, 2));
	m_theShape.AddPoint(Vec2(-2, -2));
}
