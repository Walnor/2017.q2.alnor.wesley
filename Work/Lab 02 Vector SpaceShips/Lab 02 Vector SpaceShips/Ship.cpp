#include "Ship.h"



Ship::Ship()
{
	SetShape();
}

Ship::Ship(float x, float y)
{
	SetShape();
	m_Position = Vec2(x, y);
}


Ship::~Ship()
{
}


bool Ship::Update(float dt)
{
	if (m_Speed >= 500)
	{
		m_Speed = 500;
	}

	m_Volocity = m_Direction * m_Speed;

	m_Position = m_Position + (Vec2(m_Volocity.x, m_Volocity.y) * dt);
	m_theShape.setOffset(m_Position);
	return false;
}

void Ship::Draw(Core::Graphics & g)
{
	m_theShape.Draw(g);
}

void Ship::SetShape()
{

	m_theShape.AddPoint(Vec2(1, -5));
	m_theShape.AddPoint(Vec2(2, -4));
	m_theShape.AddPoint(Vec2(2, -2));
	m_theShape.AddPoint(Vec2(1, -1));
	m_theShape.AddPoint(Vec2(1, 1));
	m_theShape.AddPoint(Vec2(2, 0));
	m_theShape.AddPoint(Vec2(3, -2));
	m_theShape.AddPoint(Vec2(3, 1));
	m_theShape.AddPoint(Vec2(1, 3));
	m_theShape.AddPoint(Vec2(1, 4));
	m_theShape.AddPoint(Vec2(0.5f, 4));
	m_theShape.AddPoint(Vec2(0.5f, 3.5f));
	m_theShape.AddPoint(Vec2(-0.5f, 3.5f));
	m_theShape.AddPoint(Vec2(-0.5f, 4));
	m_theShape.AddPoint(Vec2(-1, 4));
	m_theShape.AddPoint(Vec2(-1, 3));
	m_theShape.AddPoint(Vec2(-3, 1));
	m_theShape.AddPoint(Vec2(-3, -2));
	m_theShape.AddPoint(Vec2(-2, 0));
	m_theShape.AddPoint(Vec2(-1, 1));
	m_theShape.AddPoint(Vec2(-1, -1));
	m_theShape.AddPoint(Vec2(-2, -2));
	m_theShape.AddPoint(Vec2(-2, -4));
	m_theShape.AddPoint(Vec2(-1, -5));

	m_theShape.setScale(5);
}
