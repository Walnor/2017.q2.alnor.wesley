#include "ObjectFactory.h"
#include"Wall.h"


GameObject ObjectFactory::MakeRandomObject()
{
	GameObject newObject(1280/2, 720/2);

	newObject.setVolocity(Vec2(static_cast<float>((rand() % 10000) - 5000)/10.0f, static_cast<float>((rand() % 10000) - 5000) / 10.0f));
	newObject.giveColor(rand() % 255, rand() % 255, rand() % 255);

	return newObject;
}

void ObjectFactory::AddWalls(ObjectManager * target, Vec2 toAdd[], int ArraySize)
{
	for (int j = 1; j < ArraySize; j++)
	{
		Wall add(toAdd[j - 1], toAdd[j]);
		target->AddWall(add);
	}
}
