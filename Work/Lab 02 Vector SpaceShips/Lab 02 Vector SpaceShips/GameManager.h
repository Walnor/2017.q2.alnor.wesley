#pragma once
#include "ObjectFactory.h"


class GameManager
{
public:
	GameManager(int width = 800, int height = 800);
	~GameManager();

	bool IsInitialized() const { return m_isInitialized; }
	bool RunGame();

private:
	bool Initialize();
	bool Shutdown();

private:
	bool m_isInitialized{ false };
	int m_screenWidth;
	int m_screenHeight;
};

