#include "Vec2.h"
#include<cmath>


namespace Engine
{
	Vec2::~Vec2()
	{
	}

	bool Vec2::operator==(const Vec2 & right) const
	{
		return x == right.x ? y == right.y ? true : false : false;
	}

	bool Vec2::operator!=(const Vec2 & right) const
	{
		return x == right.x ? y == right.y ? false : true : true;

	}

	Vec2 Engine::Vec2::operator+(const Vec2 & right) const
	{
		return Vec2(x + right.x, y + right.y);
	}

	Vec2 Vec2::operator+() const
	{
		return Vec2(x,y);
	}

	Vec2 Engine::Vec2::operator-(const Vec2 & right) const
	{
		return Vec2(x - right.x, y - right.y);
	}

	Vec2 Vec2::operator-() const
	{
		return Vec2(-x,-y);
	}

	float Vec2::operator*(const Vec2 & right) const
	{
		float a = x * right.x;
		float b = y * right.y;
		return a + b;
	}

	Vec2 Vec2::operator*(const float & right) const
	{
		return Vec2(x * right, y * right);
	}

	Vec2 Vec2::operator*(const int & right) const
	{
		return operator*(static_cast<float>(right));
	}

	Vec2 Vec2::operator*(const double & right) const
	{
		return operator*(static_cast<float>(right));
	}

	Vec2 Vec2::operator/(const Vec2 & right) const
	{
		return Vec2(x / right.x, y / right.y);
	}

	Vec2 Vec2::operator/(const float & right) const
	{
		return Vec2(x / right, y / right);
	}

	Vec2 Vec2::operator/(const int & right) const
	{
		return operator/(static_cast<float>(right));
	}

	Vec2 Vec2::operator/(const double & right) const
	{
		return operator/(static_cast<float>(right));
	}
	
	Vec2 Vec2::ScaleByVector(const Vec2 & right) const
	{
		return Vec2(x * right.x, y * right.y);
	}

	Vec2 Vec2::PerpCW() const
	{
		return Vec2(y, -x);
	}

	Vec2 Vec2::PerpCCW() const
	{
		return Vec2(-y, x);
	}

	Vec2 Vec2::Normalize() const
	{
		if ((x == 0.0f) && (y == 0.0f)) return Vec2(0.0f,0.0f);

		float a = x * x + y * y;
		a = sqrt(a);

		return Vec2( x/a , y/a );
	}
	Vec2 Vec2::Lerp(Vec2 other, float Beta) const
	{
		Vec2 AVect = operator*(1.0f - Beta);
		Vec2 BVect = other * Beta;

		AVect = AVect + BVect;

		return AVect;
	}

	float Vec2::Length() const
	{
		return sqrt((x * x) + (y * y));
	}

	float Vec2::LengthSquared() const
	{
		return (x * x) + (y * y);
	}


	float Vec2::Dot(Vec2 other) const
	{
		return Vec2(x,y) * other;
	}

	float Vec2::Cross(Vec2 other) const
	{
		return (x * other.y) - (y * other.x);
	}

	Vec2 Vec2::Project(Vec2 other) const
	{
		//Vec2 This(x, y);
		Vec2 toReturn = this->Normalize() * (*this * other.Normalize());
		return toReturn;
	}

	std::ostream & Vec2::Output(std::ostream & os) const
	{
		os <<"["<< x << ", " << y << "]";

		return os;
	}

}
