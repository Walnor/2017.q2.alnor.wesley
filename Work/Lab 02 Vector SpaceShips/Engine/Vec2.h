#pragma once
#include<iostream>
#include<ExportHeader.h>

namespace Engine
{
	// Vec2 is an immutable vector class, except for assignment
	class ENGINE_SHARED Vec2
	{
	public:
		Vec2()						: Vec2(0.0f, 0.0f) {}
		Vec2(float xx, float yy)    : x(xx), y(yy) {}
		~Vec2();

		bool operator==(const Vec2& right) const;
		bool operator!=(const Vec2& right) const;

		Vec2 operator+(const Vec2& right) const;
		Vec2 operator+() const;

		Vec2 operator-(const Vec2& right) const;
		Vec2 operator-() const;

		float operator*(const Vec2& right) const;
		Vec2 operator*(const float& right) const;
		Vec2 operator*(const int& right) const;
		Vec2 operator*(const double& right) const;

		Vec2 operator/(const Vec2& right) const;
		Vec2 operator/(const float& right) const;
		Vec2 operator/(const int& right) const;
		Vec2 operator/(const double& right) const;

		Vec2 ScaleByVector(const Vec2& right) const;

		Vec2 PerpCW() const;
		Vec2 PerpCCW() const;

		Vec2 Normalize() const;
		Vec2 Lerp(Vec2 other, float Beta) const;
		float Length() const;
		float LengthSquared() const;

		float* Pos() { return &x; }
		float Dot(Vec2 other) const;
		float Cross(Vec2 other) const;

		Vec2 Project(Vec2 other) const;

		std::ostream & Output(std::ostream & os) const;

		friend Vec2 operator*(float left, Vec2& other);
		friend float Dot(Vec2& a, Vec2& b);
		friend float Cross(Vec2& a, Vec2& b);
		friend Vec2 Lerp(Vec2& a, Vec2& b, float beta);

	public:
		float x, y;
	};

	inline std::ostream& operator<< (std::ostream& os, const Vec2& v)
	{
		v.Output(os);

		return os;
	}

	inline Vec2 operator*(float left, Vec2& right)
	{
		return right * left;
	}

	inline float Dot(Vec2& a, Vec2& b)
	{
		return a.Dot(b);
	}

	inline float Cross(Vec2& a, Vec2& b)
	{
		return a.Cross(b);
	}

	inline Vec2 Lerp(Vec2 & a, Vec2 & b, float beta)
	{
		return a.Lerp(b, beta);
	}

}

