#include "stdafx.h"
#include "CppUnitTest.h"
#include "Mat2.h"

using namespace Microsoft::VisualStudio::CppUnitTestFramework;
using namespace Engine;
namespace Mat2Test
{
	TEST_CLASS(Mat2Test)
	{
	public:
		TEST_METHOD(IsEqual)
		{
			Mat2 a(1.0f);
			Mat2 b(1.0f, 0, 0, 1.0f);
			Assert::IsTrue(a == b);
		}
		TEST_METHOD(AddMat2)
		{
			Mat2 a(2.0f);
			Mat2 b(1.0f, 0, 0, 5.0f);
			Mat2 c = a + b;
			Mat2 d(2.0f + 1.0f, 0, 0, 5.0f + 2.0f);

			Assert::IsTrue(c == d);
		}
		TEST_METHOD(SubMat2)
		{
			Mat2 a(2.0f);
			Mat2 b(1.0f, 0, 0, 5.0f);
			Mat2 c = a - b;
			Mat2 d(1.0f, 0, 0, -3.0f);

			Assert::IsTrue(c == d);
		}
		TEST_METHOD(MultMat2)
		{
			Mat2 a(2.0f);
			Mat2 b(1.0f, 0, 0, 5.0f);
			Mat2 c = a * b;
			Mat2 d(2.0f, 0, 0, 10.0f);

			Assert::IsTrue(c == d);
		}
		TEST_METHOD(MultMat2Vec2)
		{
			Vec2 a(2.0f, 1.0f);
			Mat2 b(1.0f, 0, 0, 5.0f);
			Mat2 c = b * a;
			Mat2 d(2.0f, 0, 0, 5.0f);

			Assert::IsTrue(c == d);
		}
		TEST_METHOD(MultMat2Float)
		{
			Mat2 a(2.0f);
			float b = 3.0f;
			Mat2 c = a * b;
			Mat2 d(6.0f, 0, 0, 6.0f);

			Assert::IsTrue(c == d);
		}
		TEST_METHOD(Rotation)
		{
			Mat2 a = Mat2::Rotate(3.141592741f / 4);
			Mat2 b(cos(3.141592741f / 4), -sin(3.141592741f / 4), sin(3.141592741f / 4), cos(3.141592741f / 4));

			Assert::IsTrue(a == b);
		}
		TEST_METHOD(Scale)
		{
			Mat2 a = Mat2::Scale(4);
			Mat2 b(4);

			Assert::IsTrue(a == b);
		}
		TEST_METHOD(Negitive)
		{
			Mat2 a(2, 4, -3, 1);
			Mat2 b(-2, -4, 3, -1);

			Assert::IsTrue(-a == b);
		}
	};
}