#include "ObjectManager.h"
#include"Camera.h"

namespace
{
	PlayerInput player;
}

ObjectManager::ObjectManager()
{
}

ObjectManager::~ObjectManager()
{
}

bool ObjectManager::Update(float dt)
{
	Camera::Update();
	for (int j = 0; j < m_NumOfObjects; j++)
	{
		m_Objects[j].Update(dt);
		Bounce(m_Objects[j]);
	}
	if (m_player.isAlive())
	{
		player.Update(dt);
		bool multipleWalls = false;

		for (int j = 0; j < m_NumOfWalls; j++)
		{
			if (m_walls[j].Collision(m_player, player, multipleWalls))
			{
				multipleWalls = true;
			}
		}

		m_player.Update(dt);
		m_playerTurret.Update(dt);

		//Wrap(m_player);
	}
	//for (int j = 0; j < static_cast<int>(m_Enemies.size()); j++)
	//{
	//	if (!m_Enemies.at(j).isAlive())
	//	{
	//		m_Enemies.erase(m_Enemies.begin() + j);
	//		m_Enemies.shrink_to_fit();
	//		m_PShips.shrink_to_fit();
	//		j--;
	//	}
	//}
	//for (int j = 0; j < static_cast<int>(m_Enemies.size()); j++)
	//{
	//	for (int k = 0; k < m_NumOfWalls; k++)
	//	{
	//		m_walls[k].Collision(m_Enemies.at(j));
	//	}
	//	m_Enemies.at(j).Update(dt);
	//}
	for (int j = 0; j < static_cast<int>(m_PShips.size()); j++)
	{
		if (!m_PShips.at(j).isAlive())
		{
			m_PShips.erase(m_PShips.begin() + j);
		}
	}
	m_PShips.shrink_to_fit();
	for (int j = 0; j < static_cast<int>(m_PShips.size()); j++)
	{
		if (m_PShips.at(j).isAlive())
		{
			for (int k = 0; k < m_NumOfWalls; k++)
			{
				m_walls[k].Collision(m_PShips.at(j));
			}
			if (m_player.isAlive())
				m_PShips.at(j).setTargetLocation(m_player.GetPosition());
			else
				m_PShips.at(j).setTargetLocation(Vec2(-10000, -10000));

			m_PShips.at(j).Update(dt);
		}
	}
	for (int j = 0; j < static_cast<int>(m_Links.size()); j++)
	{
		m_Links.at(j).Update(dt);
	}
	for (int j = 0; j < static_cast<int>(m_ChainLinks.size()); j++)
	{
		m_ChainLinks.at(j).Update(dt);
	}
	for (int j = 0; j < static_cast<int>(m_MidBosses.size()); j++)
	{
		for (int k = 0; k < static_cast<int>(m_NumOfWalls); k++)
		{
			m_walls[k].Collision(m_MidBosses.at(j));
		}
		m_MidBosses.at(j).Update(dt);
		if (!m_MidBosses.at(j).isAlive())
		{
			m_MidBosses.erase(m_MidBosses.begin() + j);
		}
	}
	return false;
}

void ObjectManager::Draw(Core::Graphics & g)
{
	for (int j = 0; j < m_NumOfObjects; j++)
	{
		m_Objects[j].Draw(g);
	}
	//for (int j = 0; j < static_cast<int>(m_Enemies.size()); j++)
	//{
	//	m_Enemies.at(j).Draw(g);
	//}
	for (int j = 0; j < static_cast<int>(m_PShips.size()); j++)
	{
		if(m_PShips.at(j).isAlive())
		m_PShips.at(j).Draw(g);
	}
	//std::cout << m_player.isAlive() << std::endl;
	if (m_player.isAlive())
	{
		m_player.Draw(g);
		m_playerTurret.Draw(g);
	}
	for (int j = 0; j < m_NumOfWalls; j++)
	{
		m_walls[j].Draw(g);
	}
	for (int j = 0; j < static_cast<int>(m_Links.size()); j++)
	{
		m_Links.at(j).Draw(g);
	}
	for (int j = 0; j < static_cast<int>(m_ChainLinks.size()); j++)
	{
		m_ChainLinks.at(j).Draw(g);
	}
	for (int j = 0; j < static_cast<int>(m_MidBosses.size()); j++)
	{
		m_MidBosses.at(j).Draw(g);
	}
}

void ObjectManager::AddObject(GameObject toAdd)
{
	m_Objects.push_back(toAdd);
	m_NumOfObjects++;
}

void ObjectManager::AddObject(Enemy toAdd)
{
	m_Enemies.push_back(toAdd);
}

void ObjectManager::AddObject(PatrolShip toAdd)
{
	m_PShips.push_back(toAdd);
}

void ObjectManager::AddObject(Link toAdd)
{
	int multiChain = m_Links.size();
	m_Links.push_back(toAdd);

	if (multiChain != 0)
	{
		for (int j = 1; j < static_cast<int>(m_Links.size()); j++)
		{
			m_Links.at(j).Attach(m_Links.at(j - 1).getPointB());
		}
	}
}

void ObjectManager::AddObject(Chainlink toAdd)
{
	m_ChainLinks.push_back(toAdd);
}

void ObjectManager::AddObject(MidBoss toAdd, int distToTail)
{
	m_MidBosses.push_back(toAdd);

	m_MidBosses.at(m_MidBosses.size() - 1).AddTail(distToTail);
}

void ObjectManager::AddPlayer(Ship *toAdd)
{
	m_player = *toAdd;
	player.GiveShip(&m_player);
	Camera::TargetPlayerShip(&m_player);
}

void ObjectManager::AddWall(Wall toAdd)
{
	m_walls.push_back(toAdd);
	m_NumOfWalls++;
}

void ObjectManager::AddPlayerTurret()
{
	m_playerTurret.Attach(&m_player);
	player.GiveTurret(&m_playerTurret);
}

PlayerInput * ObjectManager::getPlayer()
{
	return &player;
}

void ObjectManager::Wrap(GameObject& object)
{
	if (object.GetPosition().x > m_width)
	{
		object.SetPosition(Vec2(0, object.GetPosition().y));
	}

	if (object.GetPosition().x < 0)
	{
		object.SetPosition(Vec2(m_width, object.GetPosition().y));
	}

	if (object.GetPosition().y > m_height)
	{
		object.SetPosition(Vec2(object.GetPosition().x, 0));
	}

	if (object.GetPosition().y < 0)
	{
		object.SetPosition(Vec2(object.GetPosition().x, m_height));
	}
}

void ObjectManager::Bounce(GameObject & object)
{
	if ((object.GetPosition().x > m_width) && (object.GetVolocity().x > 0))
	{
		object.setVolocity(Vec2(-object.GetVolocity().x, object.GetVolocity().y));
	}

	if ((object.GetPosition().x) < 0 && (object.GetVolocity().x < 0))
	{
		object.setVolocity(Vec2(-object.GetVolocity().x, object.GetVolocity().y));
	}

	if ((object.GetPosition().y > m_height) && (object.GetVolocity().y > 0))
	{
		object.setVolocity(Vec2(object.GetVolocity().x, -object.GetVolocity().y));
	}

	if ((object.GetPosition().y < 0) && (object.GetVolocity().y < 0))
	{
		object.setVolocity(Vec2(object.GetVolocity().x, -object.GetVolocity().y));
	}
}

float ObjectManager::DistanceFromClosestWall()
{
	float toReturn = 10000;
	for (int j = 0; j < m_NumOfWalls; j++)
	{
		float check = m_walls[j].DistanceAway(m_player);

		if (check < toReturn) toReturn = check;
	}

	return toReturn;
}

Link * ObjectManager::linkTest()
{
	//m_ChainLinks.at(m_ChainLinks.size() - 1).getPointB()
	//std::cout << *m_ChainLinks.at(m_ChainLinks.size() - 1).getPointB() << std::endl;
	return & m_Links.at(m_Links.size() - 1);
}

bool ObjectManager::Initialize()
{
	m_isInitialized = true;
	return true;
}

bool ObjectManager::Shutdown()
{
	return true;
}