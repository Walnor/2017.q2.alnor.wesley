#pragma once
#include"Core.h"
#include"Ship.h"
#include"Turret.h"

class PlayerInput
{
public:
	PlayerInput();
	PlayerInput(Ship * playerShip);
	~PlayerInput();

	void Update(float dt);	
	void Draw(Core::Graphics& g);
	void Wall(Vec2 Dir);

	void GiveShip(Ship * ship) { m_PlayerShip = ship; }
	void GiveTurret(Turret * t) { m_turret = t; }

	Turret * getTurret() { return m_turret; }

	bool fire();
private:
	Ship * m_PlayerShip{ nullptr };
	void Forwards(float dt);
	void Backwards(float dt);
	void RotCCW(float dt);
	void RotCW(float dt);

	void friction(float dt);
private:
	float & m_Speed;
	Vec2 & m_Direction;
	Turret * m_turret{ nullptr };
};

