#pragma once
#include"ObjectManager.h"
class fpsUI
{
public:
	fpsUI();
	~fpsUI();

	bool Update(float dt);
	void Draw(Core::Graphics& g);

	void giveObjMan(ObjectManager * obj) { m_ObjManagerPnt = obj; }
private:
	float m_framesPerSec{ 0 };
	float m_MilisecPerFrame{ 0 };

private:
	ObjectManager * m_ObjManagerPnt;
};

