#pragma once
#include "Ship.h"
#include "Vec2.h"
class Camera
{
public:
	static void TargetPlayerShip(Ship * s);

	static bool Update();

	static Vec2 CameraOffset();

	static Ship * PlayerShip();

private:
	Ship * m_playerShip{ nullptr };

	Vec2 m_Position{ 0,0 };
};

