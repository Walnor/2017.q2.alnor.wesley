#include "MidBoss.h"
#include "Camera.h"
#include"MisileManager.h"

MidBoss::MidBoss(float x, float y)
{
	m_Position = Vec2(x, y);
	m_Speed = 300.0f;
	setShape();

	setHealth(500);

	gun1.Attach(&TurretBase1);
	gun2.Attach(&TurretBase2);
}

MidBoss::MidBoss(Vec2 pos)
{
	m_Position = pos;
	m_Speed = 300.0f;
	setShape();

}

MidBoss::~MidBoss()
{
}

bool MidBoss::Update(float dt)
{
	HitRadius = static_cast<float>(15 * m_Scale);

	gun1.Attach(&TurretBase1);
	gun2.Attach(&TurretBase2);
	unitTimer += dt;

	if (m_AttachPoint != nullptr)
	{
		m_pointA = *m_AttachPoint;
		Vec2 DirVector = m_pointA - m_pointB;
		m_Rotation = (atan2(DirVector.y, DirVector.x) * 57.2958);
		m_pointB = m_Position - (Mat3::Rotate(m_Rotation) * Vec2(static_cast<float>(30 * m_Scale), 0));
		m_Position = m_pointA;

		if (unitTimer <= 0 || unitTimer >= 0.5f)
		{
			unitTimer = 0.0f;
			if (((rand() % 100) > 20) && ((m_Position - Camera::PlayerShip()->GetPosition()).Length() < 1400.0f))
			{
				MisileManager::BossFire(gun1);
				MisileManager::BossFire(gun2);
			}
		}
	}
	else
	{
		if(unitTimer <= 0 || unitTimer >= 0.5f)
		{
			if((rand() % 100) > 80)
			m_Direction = (m_Direction + Vec2(static_cast<float>(rand() % 1000 - 500), static_cast<float>(rand() % 1000 - 500)).Normalize()).Normalize();
			unitTimer = 0.0f;
			if (((rand() % 100) > 20) && ((m_Position - Camera::PlayerShip()->GetPosition()).Length() < 1400.0f))
			{
				MisileManager::BossFire(gun1);
				MisileManager::BossFire(gun2);
			}
		}

		m_Volocity = m_Direction * m_Speed;
		m_Position = m_Position + (Vec2(m_Volocity.x, m_Volocity.y) * dt);
		m_pointA = m_Position;
		Vec2 DirVector = m_pointA - m_pointB;
		m_Rotation = (atan2(DirVector.y, DirVector.x) * 57.2958);
		m_pointB = m_Position - (Mat3::Rotate(m_Rotation) * Vec2(static_cast<float>(30 * m_Scale), 0));
	}


	Mat3 offset = Mat3::Translation(m_Position - Camera::CameraOffset()) * (Mat3::Rotate(m_Rotation) * Mat3::Scale(static_cast<float>(m_Scale)));
	m_theShape.setOffset(offset);
	for (int j = 0; j < static_cast<int>(chain.size()); j++)
		chain.at(j).Update(dt);
	for (int j = 0; j < static_cast<int>(Body.size()); j++)
	{
		Body.at(j).Update(dt);
	}
	TurretBase1.SetPosition(m_Position - ((Mat3::Rotate(m_Rotation) * Mat3::Scale(static_cast<float>(m_Scale))) * Vec2(15, -10)));

	TurretBase2.SetPosition(m_Position - ((Mat3::Rotate(m_Rotation) * Mat3::Scale(static_cast<float>(m_Scale))) * Vec2(15, 10)));

	gun1.Update(dt);
	gun2.Update(dt);

	if ((m_Position - Camera::PlayerShip()->GetPosition()).Length() < 1400.0f)
	{
		gun1.setDirection(-(TurretBase1.GetPosition() - Camera::PlayerShip()->GetPosition()).Normalize());
		gun2.setDirection(-(TurretBase2.GetPosition() - Camera::PlayerShip()->GetPosition()).Normalize());

	}
	return false;
}

void MidBoss::Draw(Core::Graphics & g)
{
	m_theShape.Draw(g);
	for(int j = 0; j < static_cast<int>(chain.size()); j++)
	chain.at(j).Draw(g);

	for (int j = 0; j < static_cast<int>(Body.size()); j++)
	{
		Body.at(j).Draw(g);
	}
	gun1.Draw(g);
	gun2.Draw(g);
}

void MidBoss::Attach(Vec2 * point)
{
	m_AttachPoint = point;
	m_pointA = *m_AttachPoint;
	m_Position = m_pointA;
	m_pointB = m_Position - (Mat3::Rotate(m_Rotation) * Vec2(static_cast<float>(30 * m_Scale), 0));
}

void MidBoss::AddTail(int num)
{
	m_distFromTail = num;
	if (num <= 0)
		return;
	m_Scale =8.0f/(8.0f/num);

	chain.push_back(Chainlink(nullptr, (num) * 3));
	Body.push_back(MidBoss(m_Position));
	chain.at(0).Attach(getPointB());
	Body.at(0).Attach(chain.at(0).GetAttachPoint());
	Body.at(0).AddTail(num - 1);
}

bool MidBoss::IsHit(Misile check) const
{
	Vec2 center = TurretBase1.GetPosition() + TurretBase2.GetPosition();
	center = Vec2(center.x / 2, center.y / 2);

	if ((center - check.GetPosition()).Length() < (15 * m_Scale))
	{
		return true;
	}
	else
	{
		if (m_distFromTail > 1)
		{
			return Body.at(0).IsHit(check);
		}
	}
	return false;
}

void MidBoss::setShape()
{
	m_theShape.giveColor(250, 100, 210);

	m_theShape.AddPoint(Vec2(0, 5));
	m_theShape.AddPoint(Vec2(0, -5));
	m_theShape.AddPoint(Vec2(-10, -15));
	m_theShape.AddPoint(Vec2(-20, -15));
	m_theShape.AddPoint(Vec2(-30, -5));
	m_theShape.AddPoint(Vec2(-30, 5));
	m_theShape.AddPoint(Vec2(-20, 15));
	m_theShape.AddPoint(Vec2(-10, 15));

	//m_Scale = 2.5f;

	m_pointA = m_Position + Vec2(0, 0);
	m_pointB = m_Position - Vec2(static_cast<float>(30 * m_Scale), 0);
}
