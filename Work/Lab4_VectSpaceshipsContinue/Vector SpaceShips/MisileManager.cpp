#include "MisileManager.h"
#include"PlayerInput.h"
#include "Ship.h"
#include"ObjectManager.h"
#include<vector>

namespace
{
	MisileManager BossManager;
}

MisileManager::MisileManager()
{
}

MisileManager::MisileManager(ObjectManager * obj, PlayerInput * I, Ship * PS)
{
	objects = obj;
	input = I;
	PlayerShip = PS;
}

MisileManager::~MisileManager()
{
}

void MisileManager::Update(float dt)
{
	if (input != nullptr)
	{
		if (input->fire())
		{
			newMisile(*input->getTurret(),false);
		}
	}
	std::vector<PatrolShip> * enemies = objects->getPShips();
	std::vector<MidBoss> * midBosses = objects->getMidBosses();
	for (int j = 0; j < static_cast<int>(Misiles.size()); j++)
	{
		Misiles.at(j).Update(dt);
		if (Misiles.at(j).getEvil() && PlayerShip->isAlive())
		{
			if ((PlayerShip->GetPosition() - Misiles.at(j).GetPosition()).Length() < 10.0f)
			{
				Misiles.erase(Misiles.begin() + j);
				PlayerShip->Hit(10);
			}
		}
		else
		{
			for (int k = 0; k < static_cast<int>(enemies->size()); k++)
			{
				if (enemies->at(k).isAlive())
				{
					if ((enemies->at(k).GetPosition() - Misiles.at(j).GetPosition()).Length() < enemies->at(k).HitRadius)
					{
						Misiles.at(j).kill();
						enemies->at(k).Hit(10);
					}
				}
			}

			for (int k = 0; k < static_cast<int>(midBosses->size()); k++)
			{
				if (midBosses->at(k).IsHit(Misiles.at(j)))
				{
					Misiles.erase(Misiles.begin() + j);
					midBosses->at(k).Hit(10);
				}
			}
		}
	}
	Misiles.shrink_to_fit();
	for (int k = 0; k < static_cast<int>(enemies->size()); k++)
	{
		if (enemies->at(k).Fire())
		{
			BossFire(enemies->at(k).GetTurret());// , true);
		}
	}
	for (int j = 0; j < static_cast<int>(Misiles.size()); j++)
	{
		if (!Misiles.at(j).isAlive())
		{
			Misiles.erase(Misiles.begin() + j);
		}
	}
	Misiles.shrink_to_fit();
}

void MisileManager::Draw(Core::Graphics & g)
{
	for (int j = 0; j < static_cast<int>(Misiles.size()); j++)
	{
		Misiles.at(j).Draw(g);
	}
}

void MisileManager::BossMM(ObjectManager * obj, PlayerInput * I, Ship * PS)
{
	BossManager = MisileManager(obj, I, PS);
}

void MisileManager::BossFire(Turret gun)
{
	BossManager.newMisile(gun, true);
}

void MisileManager::BossMMUpdate(float dt)
{
	BossManager.Update(dt);
}

void MisileManager::BossMMDraw(Core::Graphics & g)
{
	BossManager.Draw(g);
}

void MisileManager::newMisile(Turret gun, bool evil)
{
	Misile newMisile(gun.getPosition().x, gun.getPosition().y, evil);
	newMisile.setSpeed(600.0f);
	newMisile.setDirection(gun.getDir());
	newMisile.setRotation(atan2(gun.getDir().x, -gun.getDir().y) * 57.2958);

	Misiles.push_back(newMisile);
}
