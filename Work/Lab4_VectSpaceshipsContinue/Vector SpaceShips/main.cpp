#include"GameManager.h"
#include<time.h>

int main()
{
	time_t t = time(0);
	unsigned t2 = static_cast<unsigned>(t);
	srand(t2);

	GameManager game(1280, 720);

	if (!game.IsInitialized()) return -1;
	game.RunGame();

	return 0;
}