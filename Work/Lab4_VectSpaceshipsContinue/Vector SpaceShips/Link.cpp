#include "Link.h"
#include"Camera.h"

Link::Link(float x, float y)
{
	m_Position = Vec2(x, y);
	setShape();
}

Link::Link(Vec2 pos)
{
	m_Position = pos;
	setShape();
}

Link::~Link()
{
}

bool Link::Update(float dt)
{
	dt;
	if (m_AttachPoint != nullptr)
	{
		m_pointA = *m_AttachPoint;
		Vec2 DirVector = m_pointA - m_pointB;
		m_Rotation = (atan2(DirVector.y, DirVector.x) * 57.2958);
		m_pointB = m_Position - (Mat3::Rotate(m_Rotation) * Vec2(12, 0));
		m_Position = m_pointA;
	}

	Mat3 offset = Mat3::Translation(m_Position - Camera::CameraOffset()) * (Mat3::Rotate(m_Rotation) * Mat3::Scale(static_cast<float>(m_Scale)));
	m_theShape.setOffset(offset);
	return false;
}

void Link::Draw(Core::Graphics & g)
{
	//std::cout << "hello" << std::endl;
	m_theShape.Draw(g);
}

void Link::Attach(Vec2 * point)
{
	if (point == nullptr)
		return;
	m_AttachPoint = point;
	m_pointA = *m_AttachPoint;
	m_Position = m_pointA;
	m_pointB = m_Position - (Mat3::Rotate(m_Rotation) * Vec2(12, 0));
}

void Link::setShape()
{
	m_theShape.giveColor(150, 160, 210);

	m_theShape.AddPoint(Vec2(-6, -6));
	m_theShape.AddPoint(Vec2(0, 0));
	m_theShape.AddPoint(Vec2(-6, 6));
	m_theShape.AddPoint(Vec2(-12, 0));

	m_pointA = m_Position + Vec2(0, 0);
	m_pointB = m_Position - Vec2(12, 0);
}
