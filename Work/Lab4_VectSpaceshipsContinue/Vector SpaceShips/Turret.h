#pragma once
#include"GameObject.h"
#include"Shape.h"
class Turret
{
public:
	Turret();
	~Turret();

	void Attach(GameObject * target);

	void setDirection(Vec2 dir) { m_Direction = dir; }

	bool Update(float dt);
	void Draw(Core::Graphics& g);

	void setScale(float s) { m_scale = s; }

	Vec2 getPosition() const { return m_PointA; }
	Vec2 getDir() const{ return m_Direction; }

	bool Fire();

private:
	GameObject * m_base;

	float m_FireTimer{ 0.0f };

	Shape m_shape;
	float m_scale{ 1 };
	Vec2 m_Direction{ 0, 1.0 };
	Vec2 m_PointA{ 0,0 }, m_PointB{ 0,0 };

private:
	void setShape();
};

