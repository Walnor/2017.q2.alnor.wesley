#include "fpsUI.h"
#include"Shape.h"
#include"Camera.h"

fpsUI::fpsUI()
{
}

fpsUI::~fpsUI()
{
}

bool fpsUI::Update(float dt)
{
	static float frames = 0;
	static int frameCount = 0;
	frames += dt;
	frameCount++;

	if (frames > 1)
	{
		m_framesPerSec = (1.0f / frames) * frameCount;
		m_MilisecPerFrame = 1000.0f / m_framesPerSec;
		frameCount = 0;
		frames = 0;
	}
	return false;
}

void fpsUI::Draw(Core::Graphics & g)
{
	g.SetColor(RGB(255, 255, 255));
	char fps[12];
	snprintf(fps, sizeof fps, "FPS: %f", m_framesPerSec);

	char mpf[12];
	snprintf(mpf, sizeof mpf, "MPF: %f", m_MilisecPerFrame);

	char PHe[12];
	snprintf(PHe, sizeof PHe, "HP: %i", Camera::PlayerShip()->getHealth());

	g.DrawString(1000, 20, fps);
	g.DrawString(1000, 50, mpf);
	g.DrawString(100, 20, PHe);
}
