#pragma once
#include<vector>
#include"Link.h"
class Chainlink
{
public:
	Chainlink(Vec2 * attachPoint, int ChainCount);
	~Chainlink();

	bool Update(float dt);
	void Draw(Core::Graphics& g);

	void Attach(Vec2 * point);

	Vec2 * GetAttachPoint();
private:
	std::vector<Link> m_chain;

	bool m_firstCheck{ true };
};

