#include "Misile.h"
#include"Camera.h"

Misile::Misile()
{
}

Misile::Misile(float x, float y, bool e)
{
	m_Position = Vec2(x, y);
	isEvil = e;
	SetShape();
}


Misile::~Misile()
{
}

bool Misile::Update(float dt)
{
	m_Volocity = m_Direction * m_Speed;

	m_Position = m_Position + (Vec2(m_Volocity.x, m_Volocity.y) * dt);

	Mat3 offset = Mat3::Translation(m_Position - Camera::CameraOffset()) * (Mat3::Rotate(m_Rotation) * Mat3::Scale(static_cast<float>(m_Scale)));
	m_theShape.setOffset(offset);

	m_Life -= dt;

	return false;
}

void Misile::Draw(Core::Graphics & g)
{
	m_theShape.Draw(g);
}

bool Misile::isAlive() const
{
	if(m_Life > 0.0f)
	return true;
	return false;
}

void Misile::SetShape()
{
	m_theShape.giveColor(20, 200, 200);

	m_theShape.AddPoint(Vec2(-2, 2));
	m_theShape.AddPoint(Vec2(-2, -1));
	m_theShape.AddPoint(Vec2(-1, -2));
	m_theShape.AddPoint(Vec2(1, -2));
	m_theShape.AddPoint(Vec2(2, -1));
	m_theShape.AddPoint(Vec2(-2, -1));
	m_theShape.AddPoint(Vec2(2, -1));
	m_theShape.AddPoint(Vec2(2, 2));
	m_theShape.AddPoint(Vec2(2, -1));
	m_theShape.AddPoint(Vec2(-2, -1));


}
