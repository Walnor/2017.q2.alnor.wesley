#include "Camera.h"

namespace
{
	Camera GameCam;
}

void Camera::TargetPlayerShip(Ship * s)
{
	GameCam.m_playerShip = s;
}

bool Camera::Update()
{
	//std::cout << "hello" << std::endl;
	if (GameCam.m_playerShip != nullptr)
	{
		Vec2 PlayerPos = GameCam.m_playerShip->GetPosition() - GameCam.CameraOffset();
		float x = GameCam.m_Position.x;
		float y = GameCam.m_Position.y;
		if (PlayerPos.x <= 200)
		{
			x = GameCam.m_playerShip->GetPosition().x - 200;
		}
		if (PlayerPos.x >= 1080)
		{
			x = GameCam.m_playerShip->GetPosition().x - 1080;
		}
		if (PlayerPos.y <= 200)
		{
			y = GameCam.m_playerShip->GetPosition().y - 200;
		}
		if (PlayerPos.y >= 520)
		{
			y = GameCam.m_playerShip->GetPosition().y - 520;
		}

		GameCam.m_Position = Vec2(x, y);
	}
	return false;
}

Vec2 Camera::CameraOffset()
{
	return GameCam.m_Position;
}

Ship * Camera::PlayerShip()
{	
	return GameCam.m_playerShip;
}
