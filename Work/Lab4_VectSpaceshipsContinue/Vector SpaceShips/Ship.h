#pragma once
#include "GameObject.h"
class Ship :
	public GameObject
{
public:
	Ship();
	Ship(float x, float y);
	~Ship();

	bool isAlive() const;

	bool Update(float dt);
	void Draw(Core::Graphics& g);

	void Hit(int d) { m_Health -= d; }

	float & giveSpeed() { return m_Speed; }
	Vec2 & giveDirection() { return m_Direction; }

	int getHealth() const { return m_Health; }
	void setHealth(int h) { m_Health = h; }

	Vec2 TestLink;
private:
	void SetShape();

	int m_Health{ 120 };
};

