#pragma once
#include"Misile.h"

class ObjectManager;
class PlayerInput;
class Turret;
class Ship;

class MisileManager
{
public:
	MisileManager();
	MisileManager(ObjectManager * obj, PlayerInput * I, Ship * PS);
	~MisileManager();

	void Update(float dt);
	void Draw(Core::Graphics& g);

	static void BossMM(ObjectManager * obj, PlayerInput * I, Ship * PS);
	static void BossFire(Turret gun);
	static void BossMMUpdate(float dt);
	static void BossMMDraw(Core::Graphics& g);

private:
	void newMisile(Turret gun, bool evil);

private:
	ObjectManager * objects{ nullptr };
	PlayerInput * input{ nullptr };
	Ship * PlayerShip{ nullptr };
	std::vector<Misile> Misiles;
};

