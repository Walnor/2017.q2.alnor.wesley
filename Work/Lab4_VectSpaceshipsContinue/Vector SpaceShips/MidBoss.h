#pragma once
#include"Enemy.h"
#include"Chainlink.h"
#include"Turret.h"
#include"Misile.h"

class MidBoss :
	public Enemy
{
public:
	MidBoss(float x, float y);
	MidBoss(Vec2 pos);
	~MidBoss();

	bool Update(float dt);
	void Draw(Core::Graphics& g);

	void Attach(Vec2 * point);

	void AddTail(int num);

	bool IsHit(Misile check) const;

	Vec2 * getPointA() { return &m_pointA; }
	Vec2 * getPointB() { return &m_pointB; }
private:
	void setShape();
	float unitTimer = -0.1f;
	int m_distFromTail{ 0 };
private:
	Vec2 * m_AttachPoint{ nullptr };
	Vec2 m_pointA{ 0,0 }, m_pointB{ 0,0 };

	GameObject TurretBase1;
	GameObject TurretBase2;

	Turret gun1;
	Turret gun2;

	std::vector<Chainlink> chain;
	std::vector<MidBoss> Body;
};

