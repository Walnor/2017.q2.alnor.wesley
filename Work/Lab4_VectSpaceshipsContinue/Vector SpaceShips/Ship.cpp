#include "Ship.h"
#include"Camera.h"

Ship::Ship()
{
	SetShape();
}

Ship::Ship(float x, float y)
{
	SetShape();
	m_Position = Vec2(x, y);
}


Ship::~Ship()
{
}

bool Ship::isAlive() const
{
	return (m_Health >= 0);
}


bool Ship::Update(float dt)
{
	//std::cout << m_Health << std::endl;
	m_Volocity = m_Direction * m_Speed;

	m_Position = m_Position + (Vec2(m_Volocity.x, m_Volocity.y) * dt);

	Mat3 offset = Mat3::Translation(m_Position - Camera::CameraOffset()) * (Mat3::Rotate(m_Rotation) * Mat3::Scale(static_cast<float>(m_Scale)));
	m_theShape.setOffset(offset);

	TestLink = m_Position + (Mat3::Rotate(m_Rotation) * Mat3::Scale(static_cast<float>(m_Scale))) * (Vec2(1, -5));

	return false;
}

void Ship::Draw(Core::Graphics & g)
{
	m_theShape.Draw(g);
}

void Ship::SetShape()
{
	m_Scale = 3.0;

	m_theShape.AddPoint(Vec2(1, -5));
	m_theShape.AddPoint(Vec2(2, -4));
	m_theShape.AddPoint(Vec2(2, -2));
	m_theShape.AddPoint(Vec2(1, -1));
	m_theShape.AddPoint(Vec2(1, 1));
	m_theShape.AddPoint(Vec2(2, 0));
	m_theShape.AddPoint(Vec2(3, -2));
	m_theShape.AddPoint(Vec2(3, 1));
	m_theShape.AddPoint(Vec2(1, 3));
	m_theShape.AddPoint(Vec2(1, 4));
	m_theShape.AddPoint(Vec2(0.5f, 4));
	m_theShape.AddPoint(Vec2(0.5f, 3.5f));
	m_theShape.AddPoint(Vec2(-0.5f, 3.5f));
	m_theShape.AddPoint(Vec2(-0.5f, 4));
	m_theShape.AddPoint(Vec2(-1, 4));
	m_theShape.AddPoint(Vec2(-1, 3));
	m_theShape.AddPoint(Vec2(-3, 1));
	m_theShape.AddPoint(Vec2(-3, -2));
	m_theShape.AddPoint(Vec2(-2, 0));
	m_theShape.AddPoint(Vec2(-1, 1));
	m_theShape.AddPoint(Vec2(-1, -1));
	m_theShape.AddPoint(Vec2(-2, -2));
	m_theShape.AddPoint(Vec2(-2, -4));
	m_theShape.AddPoint(Vec2(-1, -5));

	giveColor(rand() % 255, rand() % 255, rand() % 255);
}
