#include "Chainlink.h"

Chainlink::Chainlink(Vec2 * attachPoint, int ChainCount)
{
	if (ChainCount == 0)
		return;

	for (int j = 0; j < ChainCount; j++)
	{
		m_chain.push_back(Link(0, 0));
	}
	m_chain.at(0).Attach(attachPoint);
}

Chainlink::~Chainlink()
{
}

bool Chainlink::Update(float dt)
{
	if (m_firstCheck)
	{
		for (int j = 1; j < static_cast<int>(m_chain.size()); j++)
		{
			m_chain.at(j).Attach(m_chain.at(j - 1).getPointB());
		}
		m_firstCheck = false;
	}
	for (int j = 0; j < static_cast<int>(m_chain.size()); j++)
	{
		m_chain.at(j).Update(dt);
	}
	return false;
}

void Chainlink::Draw(Core::Graphics & g)
{
	for (int j = 0; j < static_cast<int>(m_chain.size()); j++)
	{
		m_chain.at(j).Draw(g);
	}
}

void Chainlink::Attach(Vec2 * point)
{
	m_chain.at(0).Attach(point);
}

Vec2 * Chainlink::GetAttachPoint()
{
	return m_chain.at(m_chain.size() -1).getPointB();
}
