#pragma once
	class RenderUI;
class MatrixMultTab
{
public:
	MatrixMultTab(RenderUI* pRenderUI);
	~MatrixMultTab();


	bool isInitialized() const { return m_isInitialized; }

private:
	bool m_isInitialized = false;

	bool Initialize(RenderUI* pRenderUI);
	bool Shutdown();
};
