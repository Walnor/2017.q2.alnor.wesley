#pragma once
class RenderUI;
class AffineTransformTab
{
public:
	AffineTransformTab(RenderUI* pRenderUI);
	~AffineTransformTab();


	bool isInitialized() const { return m_isInitialized; }

private:
	bool m_isInitialized = false;

	bool Initialize(RenderUI* pRenderUI);
	bool Shutdown();
};


