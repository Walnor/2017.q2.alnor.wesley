#include "PerpendicularTab.h"
#include"RenderUI.h"
#include"Vec2.h"

using namespace Engine;
namespace
{
	Vec2 orignal;
	Vec2 normal;
	Vec2 Perpendicular1;
	Vec2 Perpendicular2;

	void PerpDataCallback(const PerpendicularData& data)
	{
		orignal = Vec2(data.x, data.y);
		normal = orignal.Normalize();
		Perpendicular1 = orignal.PerpCW();
		Perpendicular2 = orignal.PerpCCW();
	}
}


PerpendicularTab::PerpendicularTab(RenderUI* pRenderUI) : m_isInitialized(false)
{
	m_isInitialized = Initialize(pRenderUI);
}


PerpendicularTab::~PerpendicularTab()
{
}

bool PerpendicularTab::Initialize(RenderUI * pRenderUI)
{
	pRenderUI->setPerpendicularData(&orignal.x, &normal.x, &Perpendicular1.x, &Perpendicular2.x, PerpDataCallback);

	return false;
}

bool PerpendicularTab::Shutdown()
{
	return false;
}
