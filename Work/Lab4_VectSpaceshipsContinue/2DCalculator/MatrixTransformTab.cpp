#include "MatrixTransformTab.h"
#include"RenderUI.h"
#include"Mat3.h"
#include<iostream>

using namespace Engine;
namespace
{
	Vec2 lines[] = { Vec2(0.1f, 0.1f), Vec2(0.1f, -0.1f), Vec2(0.1f, -0.1f), Vec2(-0.1f, -0.1f),
		Vec2(-0.1f, -0.1f), Vec2(-0.1f, 0.1f), Vec2(-0.1f, 0.1f), Vec2(0.0f, 0.2f), Vec2(0.0f, 0.2f), Vec2(0.1f, 0.1f), };
	int numLines = 5;

	Mat3 scale;
	Mat3 rot;
	Mat3 trans;

	Mat3 matrices[5];

	Mat3 Current;
	void TransformCallback(const MatrixTransformData2D& data)
	{
		scale = Mat3::Scale(data.scaleX, data.scaleY);
		rot = Mat3::Rotate(data.rotate);
		trans = Mat3::Translation(data.translateX, data.translateY);

		std::cout << data.selectedMatrix << std::endl;

		matrices[data.selectedMatrix] = scale * rot * trans;

		Current = matrices[0];

		for (int j = 0; j < data.selectedMatrix; j++)
		{
			Current = Current * matrices[j+1];
		}
	}
}


MatrixTransformTab::MatrixTransformTab(RenderUI* pRenderUI) : m_isInitialized(false)
{
	m_isInitialized = Initialize(pRenderUI);
}


MatrixTransformTab::~MatrixTransformTab()
{
}

bool MatrixTransformTab::Initialize(RenderUI * pRenderUI)
{		
	pRenderUI->set2DMatrixVerticesTransformData(lines[0].Pos(), numLines, matrices[0].Pos(),
		Current.Pos() , TransformCallback);
	return false;
}

bool MatrixTransformTab::Shutdown()
{
	return false;
}
