#include<iostream>
#include"RenderUI.h"
#include"TabManager.h"
#include"AdditionTab.h"
#include"Engine.h"
#include"Vec2.h"

using namespace Engine;

int main(int argc, char** argv)
{
	std::cout << "Hello World!\n";

	if (!Engine::Initialized()) return -2;

	RenderUI render;
	TabManager tabs(&render);

	Vec2 a(3, 4);
	Vec2 b(0, 0);

	float c = sqrt((3 * 3) + (4 * 4));


	std::cout << b.Normalize() << std::endl;

	if (!render.initialize(argc, argv))	return -1;

	render.run();
	render.shutdown();

	Engine::Shutdown();

	return 0;
}