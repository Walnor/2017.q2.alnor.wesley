#pragma once

class RenderUI;
class TabManager
{
public:
	TabManager(RenderUI* pRenderUI);
	~TabManager();

	
	bool isInitialized() const { return m_isInitialized; }

private:
	bool m_isInitialized = false;

	bool Initialize(RenderUI* pRenderUI);
	bool Shutdown();
};

