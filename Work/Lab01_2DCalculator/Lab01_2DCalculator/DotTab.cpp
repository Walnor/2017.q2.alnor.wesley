#include "DotTab.h"
#include"RenderUI.h"
#include"Vec2.h"

using namespace Engine;
namespace
{
	Vec2 left;
	Vec2 right;
	Vec2 projectionVector;
	Vec2 rejectionVector;

	void DotCallback(const DotProductData& data)
	{
		left = Vec2(data.v1i, data.v1j);
		right = Vec2(data.v2i, data.v2j);
		projectionVector = data.projectOntoLeftVector ? left.Normalize() * (left * right.Normalize()) :
			right.Normalize() * (right * left.Normalize());
		rejectionVector = data.projectOntoLeftVector ? right - projectionVector : 
			left - projectionVector;
	}
}


DotTab::DotTab(RenderUI* pRenderUI) : m_isInitialized(false)
{
	m_isInitialized = Initialize(pRenderUI);
}


DotTab::~DotTab()
{
}

bool DotTab::Initialize(RenderUI * pRenderUI)
{
	pRenderUI->setDotProductData(left.Pos(), right.Pos(), projectionVector.Pos(), rejectionVector.Pos(), DotCallback);
	return false;
}

bool DotTab::Shutdown()
{
	return false;
}
