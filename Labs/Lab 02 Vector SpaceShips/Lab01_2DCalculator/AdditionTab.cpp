#include "AdditionTab.h"
#include"RenderUI.h"
#include"Vec2.h"

using namespace Engine;
namespace
{
	Vec2 left;
	Vec2 right;
	Vec2 result;
	void AdditionTabCallback(const BasicVectorEquationInfo& data)
	{
		left = Vec2(data.x1, data.y1) * data.scalar1;
		right = Vec2(data.x2, data.y2) * data.scalar2;
		result = data.add ? left + right : left - right;
	}
}


AdditionTab::AdditionTab(RenderUI* pRenderUI) : m_isInitialized(false)
{
	m_isInitialized = Initialize(pRenderUI);
}


AdditionTab::~AdditionTab()
{
}

bool AdditionTab::Initialize(RenderUI * pRenderUI)
{
	pRenderUI->setBasicVectorEquationData(AdditionTabCallback, left.Pos(), right.Pos(), result.Pos());
	return false;
}

bool AdditionTab::Shutdown()
{
	return false;
}
