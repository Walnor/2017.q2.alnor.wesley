#include "PatrolShip.h"


PatrolShip::PatrolShip()
{
	SetShape();
}

PatrolShip::PatrolShip(float x, float y)
{
	SetShape();
	m_Position = Vec2(x, y);
}


PatrolShip::~PatrolShip()
{
}

bool PatrolShip::Update(float dt)
{
	if (m_Speed >= 500)
	{
		m_Speed = 500;
	}

	if (m_waypoints > 0)
	{
		m_Speed = 300;

		if (m_target == nullptr)
		{
			int to = rand() % m_waypoints;
			m_turret.Attach(this);
			m_target = &m_points[to];
		}

		if ((m_target->GetPosition() - m_Position).Length() < 10)
		{
			int to = rand() % m_waypoints;

			m_target = &m_points[to];
		}

		if (m_turretTarget != nullptr)
		{
			m_turret.setDirection(-(this->GetPosition() - m_turretTarget->GetPosition()).Normalize());
		}

		m_Direction = (m_target->GetPosition() - m_Position).Normalize();
	}

	m_Volocity = m_Direction * m_Speed;

	m_Position = m_Position + (Vec2(m_Volocity.x, m_Volocity.y) * dt);
	m_theShape.setOffset(m_Position);
	m_turret.Update(dt);
	return false;
}

void PatrolShip::Draw(Core::Graphics & g)
{
	m_theShape.Draw(g);
	m_turret.Draw(g);
}

void PatrolShip::GiveWaypoint(Waypoint point)
{
	if (m_waypoints < 4)
	{
		m_points[m_waypoints] = point;
		m_waypoints++;
	}
}

void PatrolShip::SetShape()
{
	m_theShape.AddPoint(Vec2(-1, -3));
	m_theShape.AddPoint(Vec2(1, -3));
	m_theShape.AddPoint(Vec2(2, -1));
	m_theShape.AddPoint(Vec2(4, -1));
	m_theShape.AddPoint(Vec2(4, 2));
	m_theShape.AddPoint(Vec2(2, 2));
	m_theShape.AddPoint(Vec2(1, 4));
	m_theShape.AddPoint(Vec2(-1, 4));
	m_theShape.AddPoint(Vec2(-2, 2));
	m_theShape.AddPoint(Vec2(-4, 2));
	m_theShape.AddPoint(Vec2(-4, -1));
	m_theShape.AddPoint(Vec2(-2, -1));
	m_theShape.AddPoint(Vec2(-1, -3));

	giveColor(rand() % 255, rand() % 255, rand() % 255);

	m_theShape.setScale(8);
}
