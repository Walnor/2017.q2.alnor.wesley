#include "TabManager.h"
#include"RenderUI.h"
#include"AdditionTab.h"
#include"PerpendicularTab.h"
#include"LerpTab.h"
#include"DotTab.h"
#include"MatrixMultTab.h"
#include"AffineTransformTab.h"
#include"MatrixTransformTab.h"


TabManager::TabManager(RenderUI* pRenderUI) : m_isInitialized(false)
{
	m_isInitialized = Initialize(pRenderUI);
}


TabManager::~TabManager()
{
}

bool TabManager::Initialize(RenderUI * pRenderUI)
{
	AdditionTab additiontab(pRenderUI);
	PerpendicularTab Perpendicular(pRenderUI);
	DotTab dot(pRenderUI);
	LerpTab Lerp(pRenderUI);
	MatrixMultTab BasicMatrixMult(pRenderUI);
	AffineTransformTab AffineTransTab(pRenderUI);
	MatrixTransformTab MatrixTransTab(pRenderUI);

	return false;
}

bool TabManager::Shutdown()
{
	return false;
}
