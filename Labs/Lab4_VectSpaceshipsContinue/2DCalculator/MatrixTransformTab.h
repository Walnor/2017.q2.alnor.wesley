#pragma once
class RenderUI;
class MatrixTransformTab
{
public:
	MatrixTransformTab(RenderUI* pRenderUI);
	~MatrixTransformTab();


	bool isInitialized() const { return m_isInitialized; }

private:
	bool m_isInitialized = false;

	bool Initialize(RenderUI* pRenderUI);
	bool Shutdown();
};
