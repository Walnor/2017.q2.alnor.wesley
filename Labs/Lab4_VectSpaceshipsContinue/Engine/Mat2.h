#pragma once
#include<iostream>
#include<ExportHeader.h>
#include"Vec2.h"

namespace Engine
{
	// Mat2 is an immutable vector class, except for assignment
	class ENGINE_SHARED Mat2
	{
	public:
		Mat2();
		Mat2(float identity);
		Mat2(float a, float b, float c, float d);
		Mat2(Vec2 identity);
		Mat2(Vec2 R1, Vec2 R2);

	public:
		static Mat2 Rotate(float degrees);
		static Mat2 Scale(float scale);
		static Mat2 Scale(Vec2 scale);
		static Mat2 Scale(float scaleX, float scaleY);
		static Mat2 ScaleX(float scaleX);
		static Mat2 ScaleY(float scaleY);
		static Mat2 RotAndScale(float degrees, float scale);
		static Mat2 RotAndScale(float degrees, Vec2 scale);

	public:
		Mat2 operator*(Mat2 right) const;
		Vec2 operator*(Vec2 right) const;
		Mat2 operator*(float right) const;

		friend Mat2 operator*(float left, Mat2 right);

		Mat2 operator+(Mat2 right) const;
		Mat2 operator-(Mat2 right) const;
		Mat2 operator-() const;

		bool operator==(Mat2 right) const;

		float* Pos() { return &m_a; }

		std::ostream & Output(std::ostream & os) const;
	public:
		float m_a, m_b;
		float m_c, m_d;
	};

inline std::ostream& operator<< (std::ostream& os, const Mat2& v)
{
	v.Output(os);

	return os;
}

}