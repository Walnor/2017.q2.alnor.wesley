#include "Shape.h"

Shape::Shape()
{
}

Shape::Shape(Vec2 theShape[], int arraySize)
{
	for (int j = 0; j < arraySize; j++)
	{
		AddPoint(theShape[j]);
	}
}

Shape::~Shape()
{
}

void Shape::giveColor(int r, int g, int b)
{
	m_color = RGB(r, g, b);
}

void Shape::AddPoint(Vec2 point)
{
	m_Shape.push_back(point);
	m_DrawShape.push_back(point);
	m_points++;
}

void Shape::Draw(Core::Graphics & g)
{
	for (int j = 0; j < static_cast<int>(m_Shape.size()); j++)
	{
		m_DrawShape.at(j) = m_Offset * m_Shape.at(j);
	}
	for (int j = 0; j < static_cast<int>(m_Shape.size()); j++)
	{
		g.SetColor(getColor());
		if (j + 1 == static_cast<int>(m_Shape.size()))
		{
			g.DrawLine(m_DrawShape.at(j).x, m_DrawShape.at(j).y,
				m_DrawShape.at(0).x, m_DrawShape.at(0).y);
		}
		else
		{
			g.DrawLine(m_DrawShape.at(j).x, m_DrawShape.at(j).y,
				m_DrawShape.at(j + 1).x, m_DrawShape.at(j + 1).y);
		}
	}
}
