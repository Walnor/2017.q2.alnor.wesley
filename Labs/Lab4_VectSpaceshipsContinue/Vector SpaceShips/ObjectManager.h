#pragma once
#include"Wall.h"
#include"Turret.h"
#include"PatrolShip.h"
#include"Chainlink.h"
#include"MidBoss.h"

class PlayerInput;
class ObjectManager
{
	const int m_MaxObjects = 100;
public:
	ObjectManager();
	~ObjectManager();

	bool Update(float dt);
	void Draw(Core::Graphics& g);

	void AddObject(GameObject toAdd);
	void AddObject(Enemy toAdd);
	void AddObject(PatrolShip toAdd);
	void AddObject(Link toAdd);
	void AddObject(Chainlink toAdd);
	void AddObject(MidBoss toAdd, int distToTail);
	void AddPlayer(Ship *toAdd);
	void AddWall(Wall toAdd);

	void AddPlayerTurret();

	static PlayerInput * getPlayer();

	void Wrap(GameObject& object);
	void Bounce(GameObject& object);

	void giveDimentions(float width, float height) { m_width = width; m_height = height; }

	float DistanceFromClosestWall();

	std::vector<Enemy> * getEnemies() { return &m_Enemies; }
	std::vector<PatrolShip> * getPShips() { return & m_PShips; }
	std::vector<MidBoss> * getMidBosses() { return &m_MidBosses; }

	Wall & getWall(int index) { return m_walls[index]; }
	Ship * playerShip() { return &m_player; }

	Link * linkTest();
private:
	bool Initialize();
	bool Shutdown();

private:
	bool m_isInitialized{ false };

	float m_width, m_height;

	int m_NumOfObjects{ 0 };
	int m_NumOfWalls{ 0 };

	std::vector<Wall> m_walls;

	Ship m_player;
	Turret m_playerTurret;

	std::vector<GameObject> m_Objects;
	std::vector<Enemy> m_Enemies;
	std::vector<PatrolShip> m_PShips;
	std::vector<Link> m_Links;
	std::vector<Chainlink> m_ChainLinks;
	std::vector<MidBoss> m_MidBosses;
};

