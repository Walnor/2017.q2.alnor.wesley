#pragma once
#include "Enemy.h"
#include"Turret.h"

class PatrolShip :
	public Enemy
{
public:
	PatrolShip(float x, float y);
	PatrolShip(Vec2 position);
	~PatrolShip();

	bool Update(float dt);
	void Draw(Core::Graphics& g);

	Turret GetTurret() const { return m_gun; }
	bool Fire();

	void setTargetLocation(Vec2 t) { m_TargetLocation = t; }
private:
	void setShape();
	//GameObject GunBase;

	Turret m_gun{};
	Vec2 m_TargetLocation{0,0};

	void startup();


	float unitTimer{ -0.1f };
	float actTimer{ 3.0f };
};

