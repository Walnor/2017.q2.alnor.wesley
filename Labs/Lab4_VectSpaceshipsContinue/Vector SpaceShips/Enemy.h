#pragma once
#include "GameObject.h"
class Enemy :
	public GameObject
{
public:
	Enemy();
	Enemy(float x, float y);
	Enemy(Vec2 p);
	~Enemy();

	bool isAlive() const;
	bool Update(float dt);
	void Draw(Core::Graphics& g);

	float HitRadius{ 10.0f };

	void Hit(int damage) { m_Health -= damage; }

	void setHealth(int hp) { m_Health = hp; }
private:
	int m_Health{ 30 };


private:
	void setShape();
};

