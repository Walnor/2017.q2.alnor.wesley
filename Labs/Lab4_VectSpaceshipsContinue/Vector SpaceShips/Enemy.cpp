#include "Enemy.h"


Enemy::Enemy()
{
}

Enemy::Enemy(float x, float y)
{
	m_Position = Vec2(x, y);
	setShape();
}

Enemy::Enemy(Vec2 p)
{
	m_Position = p;
	setShape();
}

Enemy::~Enemy()
{
}

bool Enemy::isAlive() const
{
	return (m_Health > 0);
}

bool Enemy::Update(float dt)
{
	dt;
	Mat3 offset = Mat3::Translation(m_Position) * (Mat3::Rotate(m_Rotation) * Mat3::Scale(static_cast<float>(m_Scale)));
	m_theShape.setOffset(offset);
	return false;
}

void Enemy::Draw(Core::Graphics & g)
{
	m_theShape.Draw(g);
}

void Enemy::setShape()
{
	m_theShape.giveColor(200, 30, 200);

	m_theShape.AddPoint(Vec2(8, 8));
	m_theShape.AddPoint(Vec2(8, -8));
	m_theShape.AddPoint(Vec2(-8, -8));
	m_theShape.AddPoint(Vec2(-8, 8));
}
