#include "PatrolShip.h"
#include"Camera.h"

PatrolShip::PatrolShip(float x, float y)
{
	m_Position = Vec2(x, y);
	startup();
}

PatrolShip::PatrolShip(Vec2 position)
{
	m_Position = position;	
	startup();
}

PatrolShip::~PatrolShip()
{
}

bool PatrolShip::Update(float dt)
{
	if (!isAlive())
		return false;
	//if (unitTimer == -0.1f)
		m_gun.Attach(this);

	unitTimer += dt;

	m_gun.setDirection(Vec2(1, 0));

	if (unitTimer > actTimer)
	{
		unitTimer = 0.0f;
		actTimer = static_cast<float>(rand() % 800) / 100.0f;

		if ((m_Position - m_TargetLocation).Length() < 800.0f)
		{
			Vec2 getClose(static_cast<float>(rand() % 4000 - 2000) / 7.5f, static_cast<float>(rand() % 400 - 200) / 7.5f);
			m_Direction = ((m_TargetLocation + getClose) - m_Position).Normalize();
		}
		else
		{
			m_Direction = Vec2(static_cast<float>(rand() % 1000 - 500), static_cast<float>(rand() % 1000 - 500)).Normalize();
		}
	}
	if ((m_Position - m_TargetLocation).Length() < 800.0f)
	{
		m_gun.setDirection(-(m_Position - m_TargetLocation).Normalize());
	}
	m_Volocity = m_Direction * m_Speed;
	m_Position = m_Position + (Vec2(m_Volocity.x, m_Volocity.y) * dt);

	Mat3 offset = Mat3::Translation(m_Position - Camera::CameraOffset()) * (Mat3::Rotate(static_cast<float>(m_Rotation)) * Mat3::Scale(static_cast<float>(m_Scale)));
	m_theShape.setOffset(offset);
	m_gun.Update(dt/2);
	return false;
}

void PatrolShip::Draw(Core::Graphics & g)
{
	if (!isAlive())
		return;
	m_gun.Draw(g);
	m_theShape.Draw(g);
}

void PatrolShip::setShape()
{
	m_theShape.giveColor(240, 50, 50);

	m_theShape.AddPoint(Vec2(14, -14));
	m_theShape.AddPoint(Vec2(-14, -14));
	m_theShape.AddPoint(Vec2(-8, -8));
	m_theShape.AddPoint(Vec2(-14, -8));
	m_theShape.AddPoint(Vec2(-14, 8));
	m_theShape.AddPoint(Vec2(-8, 8));
	m_theShape.AddPoint(Vec2(-14, 14));
	m_theShape.AddPoint(Vec2(14, 14));
	m_theShape.AddPoint(Vec2(8, 8));
	m_theShape.AddPoint(Vec2(14, 8));
	m_theShape.AddPoint(Vec2(14, -8));
	m_theShape.AddPoint(Vec2(8, -8));
}

bool PatrolShip::Fire()
{
	if ((m_Position - m_TargetLocation).Length() < 800.0f)
	{
		return m_gun.Fire();
	}
	return false;
}

void PatrolShip::startup()
{
	m_Speed = 125.0f;
	HitRadius = 15.0f;
	setHealth(60);
	setShape();
	m_gun = Turret();
}
