#include "Turret.h"
#include<math.h>
#include"Camera.h"


Turret::Turret()
{
	setShape();
}

Turret::~Turret()
{
}

void Turret::Attach(GameObject * target)
{
	m_base = target;
}

bool Turret::Update(float dt)
{
	if (m_base != nullptr)
	{
		m_PointA = m_base->GetPosition();
	}

	m_FireTimer -= dt;

	m_PointB = m_PointA + (m_Direction * 15);

	Mat3 offset = Mat3::Translation(m_PointA - Camera::CameraOffset()) * (Mat3::Rotate(atan2(m_Direction.x, -m_Direction.y)) * Mat3::Scale(m_scale));
	m_shape.setOffset(offset);
	return false;
}

void Turret::Draw(Core::Graphics & g)
{
	//g.DrawLine(m_PointA.x, m_PointA.y, m_PointB.x, m_PointB.y);
	m_shape.Draw(g);
}

bool Turret::Fire()
{
	if (m_FireTimer < 0)
	{
		m_FireTimer = 0.25f;
		return true;
	}
	return false;
}

void Turret::setShape()
{
	m_shape.AddPoint(Vec2(0, -15));
	m_shape.AddPoint(Vec2(-2, 0));
	m_shape.AddPoint(Vec2(2, 0));
	m_shape.AddPoint(Vec2(0, -15));
	m_shape.AddPoint(Vec2(-6, -9));
	m_shape.AddPoint(Vec2(-3, -11));
	m_shape.AddPoint(Vec2(3, -11));
	m_shape.AddPoint(Vec2(6, -9));
	m_shape.AddPoint(Vec2(0, -15));
	m_shape.giveColor(255, 255, 100);
}
