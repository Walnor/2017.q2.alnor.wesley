#include "PlayerInput.h"
#include"Misile.h"
#include"Camera.h"
namespace
{
	using Core::Input;

	Vec2 nullDirection(0, 0);
	float nullspeed = 0;

	float maxSpeed = 600;
	float Acceleration = 800;
}

PlayerInput::PlayerInput() : m_Direction{nullDirection}, m_Speed{nullspeed}
{
}

//There is a warning here... I don't know how to fix it.
//nonstandard extension used: 'argument': conversion from 'Engine::Vec2' to 'Engine::Vec2 &'
PlayerInput::PlayerInput(Ship * playerShip) : m_Direction{playerShip->getDirection()}, m_Speed{ playerShip->giveSpeed() }
{
	m_PlayerShip = playerShip;
}

PlayerInput::~PlayerInput()
{
}

void PlayerInput::Wall(Vec2 Dir)
{
	m_Direction = Dir.Normalize();
	m_Speed *= m_Direction * (m_PlayerShip->GetVolocity().Normalize());

	if (m_Speed > maxSpeed)
	{
		m_Speed = maxSpeed;
	}

	m_PlayerShip->setSpeed(m_Speed);
	m_PlayerShip->setDirection(m_Direction);
}

bool PlayerInput::fire()
{
	if (Input::IsPressed(VK_SPACE))
		return m_turret->Fire();
	return false;
}

void PlayerInput::Forwards(float dt)
{
	Vec2 newVolocity = (m_PlayerShip->GetVolocity() + Mat3::Rotate(m_PlayerShip->GetRotation()) * (Vec2(0, -Acceleration))*dt);
	m_Speed = newVolocity.Length();
	m_Direction = newVolocity.Normalize();

	if (m_Speed > maxSpeed)
	{
		m_Speed = maxSpeed;
	}

	m_PlayerShip->setVolocity(m_Speed * m_Direction);
}

void PlayerInput::Backwards(float dt)
{
	Vec2 newVolocity = (m_PlayerShip->GetVolocity() + Mat3::Rotate(m_PlayerShip->GetRotation()) * (Vec2(0, Acceleration/2))*dt);
	m_Speed = newVolocity.Length();
	m_Direction = newVolocity.Normalize();

	if (m_Speed > maxSpeed)
	{
		m_Speed = maxSpeed;
	}

	m_PlayerShip->setVolocity(m_Speed * m_Direction);
}

void PlayerInput::RotCCW(float dt)
{
	m_PlayerShip->setRotation(m_PlayerShip->GetRotation() - (270.0 * dt));
}

void PlayerInput::RotCW(float dt)
{
	m_PlayerShip->setRotation(m_PlayerShip->GetRotation() + (270.0 * dt));
}

void PlayerInput::friction(float dt)
{
	m_Speed *= 1.0f - (0.999f * dt);
}



void PlayerInput::Update(float dt)
{
	if (m_PlayerShip != nullptr)
	{	
		friction(dt);

		if (Input::IsPressed(VK_UP) || Input::IsPressed(static_cast<int>('W')))
			Forwards(dt);
		if (Input::IsPressed(VK_DOWN) || Input::IsPressed(static_cast<int>('S')))
			Backwards(dt);
		if (Input::IsPressed(VK_LEFT) || Input::IsPressed(static_cast<int>('A')))
			RotCCW(dt);
		if (Input::IsPressed(VK_RIGHT) || Input::IsPressed(static_cast<int>('D')))
			RotCW(dt);

		m_PlayerShip->setSpeed(m_Speed);
		m_PlayerShip->setDirection(m_Direction);

		if (Input::IsPressed('H'))
			m_PlayerShip->setHealth(10000);
	}

	if (m_turret != nullptr)
	{
		if(Input::IsPressed(RI_MOUSE_BUTTON_1_DOWN))
		m_turret->setDirection(-((m_PlayerShip->GetPosition() - Camera::CameraOffset()) - Vec2(static_cast<float>(Input::GetMouseX()), static_cast<float>(Input::GetMouseY()))).Normalize());
	}

}

void PlayerInput::Draw(Core::Graphics & g)
{
	if(m_PlayerShip != nullptr)
	m_PlayerShip->Draw(g);
}
