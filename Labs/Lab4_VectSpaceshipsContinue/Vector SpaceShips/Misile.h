#pragma once
#include"GameObject.h"
class Misile :
	public GameObject
{
public:
	Misile();
	Misile(float x, float y, bool e);
	~Misile();

	bool Update(float dt);
	void Draw(Core::Graphics& g);

	float & giveSpeed() { return m_Speed; }
	Vec2 & giveDirection() { return m_Direction; }

	bool getEvil() const { return isEvil; }

	bool isAlive() const;

	void kill() { m_Life = -1; }
private:
	void SetShape();
	float m_Life{ 3.0f };

	bool isEvil{ false };
};

