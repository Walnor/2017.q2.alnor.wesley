#pragma once
#include "GameObject.h"
class Link :
	public GameObject
{
public:
	Link(float x, float y);
	Link(Vec2 pos);
	~Link();
	
	bool Update(float dt);
	void Draw(Core::Graphics& g);

	void Attach(Vec2 * point);

	Vec2 * getPointA() { return &m_pointA; }
	Vec2 * getPointB() { return &m_pointB; }

private:
	Vec2 * m_AttachPoint{ nullptr };
	Vec2 m_pointA{ 0,0 }, m_pointB{ 0,0 };

private:
	void setShape();
};

