#pragma once
#include "GameObject.h"
#include"ObjectManager.h"

class ObjectFactory
{
public:
	static GameObject MakeRandomObject();
	void static AddWalls(ObjectManager * target, Vec2 toAdd[], int ArraySize);
};

