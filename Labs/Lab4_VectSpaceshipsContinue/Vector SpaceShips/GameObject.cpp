#include "GameObject.h"
#include"Camera.h"

GameObject::GameObject(float x, float y)
{
	m_Position = Vec2(x, y);
}

GameObject::~GameObject()
{
}

bool GameObject::Update(float dt)
{
	m_Volocity = m_Direction * m_Speed;
	m_Position = m_Position + (Vec2(m_Volocity.x, m_Volocity.y) * dt);

	Mat3 offset = Mat3::Translation(m_Position - Camera::CameraOffset()) * (Mat3::Rotate(m_Rotation) * Mat3::Scale(static_cast<float>(m_Scale)));
	m_theShape.setOffset(offset);
	return false;
}

void GameObject::Draw(Core::Graphics & g)
{
	m_theShape.Draw(g);
}