#include "GameManager.h"
#include "MisileManager.h"
#include"Wall.h"
#include"fpsUI.h"
#include"PatrolShip.h"
#include"MidBoss.h"

namespace
{
	ObjectManager objects;
	Ship PlayerShip(500, 500);
	void DefaultStartState();

	MisileManager MisManage(&objects, ObjectManager::getPlayer(), objects.playerShip());

	void NewDefault();
	fpsUI fraps;

	bool pause = false;
	bool pKey = false;

	bool Update(float dt)
	{
		static float accumulation = 0.0f;

		accumulation += dt;

		if (Core::Input::IsPressed('P'))
		{
			pKey = true;
		}
		else
		{
			if (pKey == true)
			{
				pause = !pause;
				pKey = false;
			}
		}

		if (accumulation < 0.001)
		{
			return false;
		}

		fraps.Update(accumulation);
		if (!pause)
		{
			objects.Update(accumulation);
			MisileManager::BossMMUpdate(accumulation);
			MisManage.Update(accumulation);
		}
		accumulation = 0.0f;
		return false;
	}

	void Draw(Core::Graphics & g)
	{
		objects.Draw(g);
		MisManage.Draw(g);
		MisileManager::BossMMDraw(g);
		fraps.Draw(g);
	}
	void NewDefault()
	{
		objects.AddWall(Wall(Vec2(-420, 720), Vec2(1900, 720)));
		objects.AddWall(Wall(Vec2(1900, 720), Vec2(2600, -2280)));
		objects.AddWall(Wall(Vec2(2600, -2280), Vec2(-1220, -2280)));
		objects.AddWall(Wall(Vec2(-1220, -2280), Vec2(-620, 420)));
		objects.AddWall(Wall(Vec2(-620, 420), Vec2(-680, 480)));
		objects.AddWall(Wall(Vec2(-680, 480), Vec2(-420, 720)));
		for (int j = 0; j < 10; j++)
		{
			objects.AddObject(PatrolShip(rand() % 2200 - 300, rand() % 2700 - 2000));
		}
		MisileManager::BossMM(&objects, ObjectManager::getPlayer(), objects.playerShip());
	}

	void DefaultStartState()
	{
		PlayerShip.setVolocity(Vec2(0, 0));
		objects.AddPlayer(&PlayerShip);

		MidBoss a(600, -1800);

		objects.AddObject(a, 8);
		objects.AddPlayerTurret();

		fraps.giveObjMan(&objects);
		NewDefault();
	}
}

GameManager::GameManager(int width, int height)
	: m_screenWidth(width),
m_screenHeight(height)
{
	Initialize();
}

GameManager::~GameManager()
{
}

bool GameManager::RunGame()
{
	Core::GameLoop();
	Core::Shutdown();
	return true;
}

bool GameManager::Initialize()
{
	Core::Init("Not So Endless Space 0.11", m_screenWidth, m_screenHeight, 2500);
	Core::RegisterUpdateFn(Update);
	Core::RegisterDrawFn(Draw);
	m_isInitialized = true;

	objects.giveDimentions(static_cast<float>(m_screenWidth), static_cast<float>(m_screenHeight));
	DefaultStartState();
	return true;
}

bool GameManager::Shutdown()
{
	return true;
}
