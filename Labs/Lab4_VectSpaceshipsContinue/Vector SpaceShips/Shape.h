#pragma once
#include"Core.h"
#include"Mat3.h"
#include<vector>

typedef unsigned long  RGB;
typedef unsigned char  BYTE;
typedef unsigned short WORD;
#define RGB(r,g,b) ((COLORREF)(((BYTE)(r)|((WORD)((BYTE)(g))<<8))|(((DWORD)(BYTE)(b))<<16)))

using namespace Engine;
class Shape
{
public:
	Shape();
	Shape(Vec2 theShape[], int arraySize);
	~Shape();
	void giveColor(int r, int g, int b);
	
	void AddPoint(Vec2 point);
	RGB getColor() const { return m_color; }
	void setOffset(Mat3 offset) { m_Offset = offset; }
	void Draw(Core::Graphics& g);
private:
	RGB m_color{ RGB(255,255,255) };
	Mat3 m_Offset{ 0 };
	int m_points{0};
	std::vector<Vec2> m_Shape;
	std::vector<Vec2> m_DrawShape;
};

