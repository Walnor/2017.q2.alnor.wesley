#include "stdafx.h"
#include "CppUnitTest.h"
#include "Vec2.h"

using namespace Microsoft::VisualStudio::CppUnitTestFramework;
using namespace Engine;
namespace Vec2Test
{		
	TEST_CLASS(Vec2Test)
	{
	public:
		TEST_METHOD(IsEqual)
		{
			Vec2 a(2, 4), b(2.1f, 4.1f), c(2, 4);

			Assert::IsTrue(a == c);
			Assert::IsFalse(b == a);
		}

		TEST_METHOD(IsNotEqual)
		{
			Vec2 a(2, 4), b(2.1f, 4.1f), c(2, 4);

			Assert::IsTrue(a != b);
			Assert::IsFalse(a != c);
		}

		TEST_METHOD(AddVectors)
		{
			Vec2 a(2, 4), b(3, 1), c(5, 5), d(4,0), e(5,2);
			Assert::IsTrue(Vec2(5, 5) == a+b);
			Assert::IsTrue(Vec2(14, 7) == e+d+c);
			Assert::IsTrue(Vec2(7, 1) == b+d);
			Assert::IsTrue(Vec2(19, 12) == a+b+c+d+e);
			Assert::IsTrue(Vec2(7, 6) == a+e);
		}

		TEST_METHOD(PositiveVec)
		{ //I have no idea where or when such an opperator would be used but... meh.
			Vec2 a(2, 4);

			Assert::IsTrue(a == +a);
		}

		TEST_METHOD(SubtractVectors)
		{
			Vec2 a(2, 4), b(1, 2);

			Vec2 c(1, 2);

			Assert::IsTrue(c == (a - b));
		}

		TEST_METHOD(NegitiveVector)
		{
			Vec2 a(2, 4), b(-2, -4);

			Assert::IsTrue(b == -a);
		}

		TEST_METHOD(DotOpperator)
		{
			Vec2 a(3, 2), b(1, 5);
			float c = 13.0f;

			Assert::AreEqual(c, a*b);
		}

		TEST_METHOD(DotOpperatorPerpendicular)
		{
			Vec2 a(3, 2), b = a.PerpCW(), d = a.PerpCCW();
			float c = 0.0f;

			Assert::AreEqual(c, a*b);
			Assert::AreEqual(c, a*d);
		}

		TEST_METHOD(ScaleByFloat)
		{
			Vec2 a(1, 2), b(2, 4);
			float c = 2.0f;

			Assert::IsTrue((a * c) == b);
		}

		TEST_METHOD(ScaleByInt)
		{
			Vec2 a(1, 2), b(2, 4);
			int c = 2;

			Assert::IsTrue((a * c) == b);
		}

		TEST_METHOD(ScaleByDouble)
		{
			Vec2 a(1, 2), b(2, 4);
			double c = 2.0;

			Assert::IsTrue((a * c) == b);
		}

		TEST_METHOD(DivideByVec2)
		{
			Vec2 a(4, 8), b(4, 2), c(1, 4);

			Assert::IsTrue((a / b) == c);
		}

		TEST_METHOD(DivideByFloat)
		{
			Vec2 a(4, 8), c(2, 4);

			float b = 2.0f;

			Assert::IsTrue((a / b) == c);
		}

		TEST_METHOD(DivideByInt)
		{
			Vec2 a(4, 8), c(2, 4);

			int b = 2;

			Assert::IsTrue((a / b) == c);
		}

		TEST_METHOD(DivideByDouble)
		{
			Vec2 a(4, 8), c(2, 4);

			double b = 2.0;

			Assert::IsTrue((a / b) == c);
		}

		TEST_METHOD(ScaleByVector)
		{
			Vec2 a(2, 3), b(3,1);
			Vec2 c(6, 3);

			Assert::IsTrue(c == (a.ScaleByVector(b)));
		}

		TEST_METHOD(ClockWisePerpendicular)
		{
			Vec2 a(1, 4), c(4, -1);

			Assert::IsTrue(c == a.PerpCW());
		}

		TEST_METHOD(CounterClockWisePerpendicular)
		{
			Vec2 a(1, 4), c(-4, 1);

			Assert::IsTrue(c == a.PerpCCW());
		}

		TEST_METHOD(Normalize)
		{
			Vec2 a(3, 4);
			float c = sqrt((3 * 3) + (4 * 4));
			Vec2 b(3 / c, 4 / c);

			Assert::IsTrue(a.Normalize() == b);
		}

		TEST_METHOD(NormalizeZero)
		{
			Vec2 a(0, 0);
			Vec2 b(0.0, 0.0);

			Assert::IsTrue(a.Normalize() == b);
		}

		TEST_METHOD(Lerp)
		{
			Vec2 a(1, 2), b(5, 10);
			Vec2 c(3, 6);

			Assert::IsTrue(a.Lerp(b, 0.0f) == a);
			Assert::IsTrue(a.Lerp(b, 1.0f) == b);
			Assert::IsTrue(a.Lerp(b, 0.5f) == c);
		}

		TEST_METHOD(Length)
		{
			Vec2 a(3, 4);
			float b = 5.0f;

			Assert::AreEqual(a.Length(), b);
		}

		TEST_METHOD(LengthSquared)
		{
			Vec2 a(3, 4);
			float b = 25.0f;

			Assert::AreEqual(a.LengthSquared(), b);
		}

		TEST_METHOD(POS)
		{
			Vec2 a(1, 3);

			float *b = a.Pos();
			float c = 3.0f;

			Assert::AreEqual(c, *(b + 1));
		}

		TEST_METHOD(DotProductMember)
		{
			Vec2 a(3, 2), b(1, 5);
			float c = 13.0f;

			Assert::AreEqual(c, a.Dot(b));
		}

		TEST_METHOD(CrossProductMember)
		{
			Vec2 a(3, 2), b(1, 5);
			float c = 13.0f;

			Assert::AreEqual(c, a.Cross(b));
		}

		TEST_METHOD(ScaleFriend)
		{
			Vec2 a(2, 4), b(6, 12);
			float c = 3.0f;

			Assert::IsTrue((c * a) == b);
		}

		TEST_METHOD(DotFriend)
		{
			Vec2 a(3, 2), b(1, 5);
			float c = 13.0f;

			Assert::AreEqual(c, Dot(a,b));
		}

		TEST_METHOD(CrossFriend)
		{
			Vec2 a(3, 2), b(1, 5);
			float c = 13.0f;

			Assert::AreEqual(c, Cross(a,b));
		}

		TEST_METHOD(LerpFriend)
		{
			Vec2 a(1, 2), b(5, 10);
			Vec2 c(3, 6);
			Assert::IsTrue(Engine::Lerp(a, b, 0.0f) == a);
			Assert::IsTrue(Engine::Lerp(a, b, 1.0f) == b);
			Assert::IsTrue(Engine::Lerp(a, b, 0.5f) == c);
		}
	};
}