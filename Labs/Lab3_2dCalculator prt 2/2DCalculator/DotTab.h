#pragma once
class RenderUI;
class DotTab
{
public:
	DotTab(RenderUI* pRenderUI);
	~DotTab();


	bool isInitialized() const { return m_isInitialized; }

private:
	bool m_isInitialized = false;

	bool Initialize(RenderUI* pRenderUI);
	bool Shutdown();
};