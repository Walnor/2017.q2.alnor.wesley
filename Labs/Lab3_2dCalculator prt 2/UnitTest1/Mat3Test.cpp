#include "stdafx.h"
#include "CppUnitTest.h"
#include "Mat3.h"

using namespace Microsoft::VisualStudio::CppUnitTestFramework;
using namespace Engine;
namespace Mat3Test
{
	TEST_CLASS(Mat3Test)
	{
	public:
		TEST_METHOD(IsEqual)
		{
			Mat3 a(5.0f);
			Mat3 b(5.0f, 0, 0, 5.0f);
			Assert::IsTrue(a == b);
		}
		TEST_METHOD(MultMat3)
		{
			Mat3 a(2.0f);
			Mat3 b(1.0f, 0, 0, 5.0f);
			Mat3 c = a * b;
			Mat3 d(2.0f, 0, 0, 10.0f);

			Assert::IsTrue(c == d);
		}
		TEST_METHOD(MultMat3Vec2)
		{
			Vec2 a(2.0f, 1.0f);
			Mat3 b(1.0f, 0, 0, 5.0f);
			Vec2 c = b * a;
			Vec2 d(2.0f, 5.0f);

			Assert::IsTrue(c == d);
		}
		TEST_METHOD(Rotation)
		{
			Mat3 a = Mat3::Rotate(3.141592741f / 4);
			Mat3 b(cos(3.141592741f / 4), -sin(3.141592741f / 4), sin(3.141592741f / 4), cos(3.141592741f / 4));

			Assert::IsTrue(a == b);
		}
		TEST_METHOD(Scale)
		{
			Mat3 a = Mat3::Scale(4);
			Mat3 b(4);

			Assert::IsTrue(a == b);
		}
		TEST_METHOD(Translate)
		{
			Mat3 a = Mat3::Translation(3.0f, 5.0f);
			Mat3 c = Mat3::Translation(Vec2(3, 5));
			Mat3 b(1, 0, 3.0f, 0, 1, 5.0f, 0, 0, 1);

			Assert::IsTrue(a == b);
			Assert::IsTrue(c == b);
		}
	};
}