#pragma once
#include"ExportHeader.h"

namespace Engine
{
	ENGINE_SHARED bool Initialized();
	ENGINE_SHARED bool Shutdown();
}