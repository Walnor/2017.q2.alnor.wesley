#include "Mat3.h"

namespace Engine
{

	Mat3::Mat3() : 
		m_a{ 0 }, m_b{ 0 }, m_c{ 0 },
		m_d{ 0 }, m_e{ 0 }, m_f{ 0 },
		m_g{ 0 }, m_h{ 0 }, m_i{ 1 }
	{
	}

	Mat3::Mat3(float id) : 
		m_a{ id }, m_b{ 0 }, m_c{ 0 },
		m_d{ 0 }, m_e{ id }, m_f{ 0 },
		m_g{ 0 }, m_h{ 0 }, m_i{ 1 }
	{
	}

	Mat3::Mat3(Vec2 ID) : 
		m_a{ ID.x }, m_b{ 0 }, m_c{ 0 },
		m_d{ 0 }, m_e{ ID.y }, m_f{ 0 },
		m_g{ 0 }, m_h{ 0 }, m_i{ 1 }
	{
	}

	Mat3::Mat3(float a, float b, float d, float e) : 
		m_a{ a }, m_b{ b }, m_c{ 0 },
		m_d{ d }, m_e{ e }, m_f{ 0 },
		m_g{ 0 }, m_h{ 0 }, m_i{ 1 }
	{
	}

	Mat3::Mat3(float a, float b, float c, float d, float e, float f, float g, float h, float i) :
		m_a{ a }, m_b{ b }, m_c{ c },
		m_d{ d }, m_e{ e }, m_f{ f },
		m_g{ g }, m_h{ h }, m_i{ i }
	{
	}


	Mat3::~Mat3()
	{
	}

	Mat3 Mat3::Rotate(float degrees)
	{
		return Mat3(cos(degrees), -sin(degrees), sin(degrees), cos(degrees));
	}

	Mat3 Mat3::Scale(float scale)
	{
		return Mat3(scale, 0, 0, scale);
	}

	Mat3 Mat3::Scale(Vec2 scale)
	{
		return Mat3(scale.x, 0, 0, scale.y);
	}

	Mat3 Mat3::Scale(float scaleX, float scaleY)
	{
		return Mat3(scaleX, 0, 0, scaleY);
	}

	Mat3 Mat3::ScaleX(float scaleX)
	{
		return Mat3(scaleX, 0, 0, 1);
	}

	Mat3 Mat3::ScaleY(float scaleY)
	{
		return Mat3(1, 0, 0, scaleY);
	}

	Mat3 Mat3::RotAndScale(float degrees, float scale)
	{
		return Mat3(scale, 0, 0, scale) * Mat3(cos(degrees), -sin(degrees), sin(degrees), cos(degrees));
	}

	Mat3 Mat3::RotAndScale(float degrees, Vec2 scale)
	{
		return Mat3(scale.x, 0, 0, scale.y) * Mat3(cos(degrees), -sin(degrees), sin(degrees), cos(degrees));
	}

	Mat3 Mat3::Translation(float x, float y)
	{
		return Mat3(
			1, 0, x,
			0, 1, y,
			0, 0, 1
		);
	}

	Mat3 Mat3::Translation(Vec2 t)
	{
		return Mat3(
			 1 , 0 , t.x ,
			 0 , 1 , t.y ,
			 0 , 0 , 1
		);
	}

	Mat3 Mat3::operator*(Mat3 right) const
	{
		float a = (m_a * right.m_a) + (m_b * right.m_d) + (m_c * right.m_g);
		float b = (m_a * right.m_b) + (m_b * right.m_e) + (m_c * right.m_h);
		float c = (m_a * right.m_c) + (m_b * right.m_f) + (m_c * right.m_i);
		float d = (m_d * right.m_a) + (m_e * right.m_d) + (m_f * right.m_g);
		float e = (m_d * right.m_b) + (m_e * right.m_e) + (m_f * right.m_h);
		float f = (m_d * right.m_c) + (m_e * right.m_f) + (m_f * right.m_i);
		float g = (m_g * right.m_a) + (m_h * right.m_d) + (m_i * right.m_g);
		float h = (m_g * right.m_b) + (m_h * right.m_e) + (m_i * right.m_h);
		float i = (m_g * right.m_c) + (m_h * right.m_f) + (m_i * right.m_i);
		
		return Mat3(a, b, c, d, e, f, g, h, i);
	}

	Vec2 Mat3::operator*(Vec2 right) const
	{
		float x = ((right.x * m_a) + (right.y * m_b) + m_c);
		float y = ((right.x * m_d) + (right.y * m_e) + m_f);

		return Vec2(x,y);
	}

	Mat3 Mat3::Mult(Vec2 Vec) const
	{
		Mat3 other(Vec);
		return operator*(other);
	}

	bool Mat3::operator==(Mat3 right) const
	{
		if (m_a != right.m_a)
			return false;
		if (m_b != right.m_b)
			return false;
		if (m_c != right.m_c)
			return false;
		if (m_d != right.m_d)
			return false;
		if (m_e != right.m_e)
			return false;
		if (m_f != right.m_f)
			return false;
		if (m_g != right.m_g)
			return false;
		if (m_h != right.m_h)
			return false;
		if (m_i != right.m_i)
			return false;
		return true;
	}

	std::ostream & Mat3::Output(std::ostream & os) const
	{
		os << "[ { " << m_a << "  " << m_b << "  " << m_c << " } { " << m_d << "  " << m_e << "  " << m_f << " } { " << m_g << "  " << m_h << "  " << m_i << " } ]";
		return os;
	}

}