#pragma once
#include "Ship.h"
#include "PlayerInput.h"
class Wall :
	public GameObject
{
public:
	Wall() {}

	Wall(Vec2 pointA, Vec2 pointB);
	~Wall();

	void Draw(Core::Graphics& g);


	bool Collision(GameObject & ship, PlayerInput & controller, bool multipleWalls);
	float DistanceAway(GameObject & ship);

	Vec2 GetPointA() const;
	Vec2 GetPointB() const;

private:
	Vec2 m_PointA, m_PointB;

	int m_NumOfConnectedWalls = 0;

	Wall * m_ConnectedWalls[2];

};

