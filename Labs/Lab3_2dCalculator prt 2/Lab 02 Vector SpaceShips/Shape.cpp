#include "Shape.h"
#include <iostream>

Shape::Shape()
{
}

Shape::Shape(Vec2 theShape[], int arraySize)
{
	for (int j = 0; j < arraySize; j++)
	{
		AddPoint(theShape[j]);
	}
}

Shape::~Shape()
{
}

void Shape::giveColor(int r, int g, int b)
{
	m_color = RGB(r, g, b);
}

void Shape::AddPoint(Vec2 point)
{
	m_Shape.push_back(point);
	m_points++;
}

void Shape::Draw(Core::Graphics & g)
{
	for (int j = 0; j < m_Shape.size(); j++)
	{
		g.SetColor(getColor());
		if (j + 1 == m_Shape.size())
		{
			g.DrawLine((m_Shape.at(j).x * m_scale) + m_Offset.x, (m_Shape.at(j).y * m_scale) + m_Offset.y,
				(m_Shape.at(0).x * m_scale) + m_Offset.x, (m_Shape.at(0).y * m_scale) + m_Offset.y);
		}
		else
		{
			g.DrawLine((m_Shape.at(j).x * m_scale) + m_Offset.x, (m_Shape.at(j).y * m_scale) + m_Offset.y,
				(m_Shape.at(j + 1).x * m_scale) + m_Offset.x, (m_Shape.at(j + 1).y * m_scale) + m_Offset.y);
		}
	}
}
