#pragma once
#include"GameObject.h"
class Turret
{
public:
	Turret();
	~Turret();

	void Attach(GameObject * target);

	void setDirection(Vec2 dir) { m_Direction = dir; }

	bool Update(float dt);
	void Draw(Core::Graphics& g);

private:
	GameObject * m_base;

	Vec2 m_Direction{ 0.5, 0.5 };
	Vec2 m_PointA{ 0,0 }, m_PointB{ 0,0 };
};

