#include "GameObject.h"
#include<iostream>

GameObject::GameObject(float x, float y)
{
	m_Position = Vec2(x, y);
}

GameObject::~GameObject()
{
}

bool GameObject::Update(float dt)
{
	m_Volocity = m_Direction * m_Speed;
	m_Position = m_Position + (Vec2(m_Volocity.x, m_Volocity.y) * dt);
	return false;
}

void GameObject::Draw(Core::Graphics & g)
{
	float xO = m_Position.x;
	float yO = m_Position.y;

	m_theShape.setOffset(m_Position);
	m_theShape.Draw(g);
}
