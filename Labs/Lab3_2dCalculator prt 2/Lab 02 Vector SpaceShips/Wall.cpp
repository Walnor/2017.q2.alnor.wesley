#include "Wall.h"

namespace
{
	bool oldDefectiveCollision(Vec2 a, Vec2 b, Vec2 vol)
	{ //This has been moved here, it works but it has trouble when two walls are touching at the points
		//This often results in the ship being able to get through the walls if it goes to the corners.
		//This can also happen in the new verson but it is much less frequent and much harder to do even when
		//intentionally trying.
		float PositionTest = a * b;
		bool collision = false;

		if (PositionTest > -1.1 && PositionTest < -0.99)
		{
			Vec2 ProjectDirection = a - b;
			float MovingInto = vol.Normalize().Cross(ProjectDirection.Normalize() * 2);
			float cross = a.Cross(b);

			if ((PositionTest <= -0.995 && PositionTest >= -1.1) && MovingInto < 0 && cross < 0) {
				collision = true;
			}
			if ((PositionTest <= -0.995 && PositionTest >= -1.1) && MovingInto > 0 && cross > 0) {
				collision = true;
			}
		}
		return collision;
		//If collision continues to be a problem then I will rebuild it again but use delta time
		//to create a line between the ship position it is currently in and the position it is trying
		//to go to. this will likely be much more accurate then what i currently have but more taxing 
		//on the computer.
	}
	
	float DistanceAwayOld(GameObject & ship, Wall wall)
	{//This was the origonal formula that was used... I may have messed up when i wrote it down
		//Anyway the new one is basically the same just more accurate.
		Vec2 a = wall.GetPointA();
		Vec2 b = wall.GetPointB();

		Vec2 c = ship.GetPosition();

		float x0 = c.x;
		float y0 = c.y;

		float x1 = a.x;
		float y1 = a.y;

		float x2 = b.x;
		float y2 = b.y;

		float top = ((y2 - y1) * x0) - ((x2 - x1) * y0) + ((x2 * y1) - (y2 * x1));

		if (top < 0)
		{
			top = -top;
		}

		float bot = sqrt(((y2 - y1) * (y2 - y1)) + ((x2 - x1) * (x2 - x1)));

		return top / bot;
	}
}

Wall::Wall(Vec2 pointA, Vec2 pointB)
{
	m_Volocity = Vec2(0, 0);

	Vec2 points[] = { pointA, pointB };
	m_theShape = Shape(points, 2);

	m_theShape.giveColor(255, 0, 0);

	m_PointA = pointA;
	m_PointB = pointB;
}

Wall::~Wall()
{
}

void Wall::Draw(Core::Graphics & g)
{
	m_theShape.Draw(g);
}


bool Wall::Collision(GameObject & ship, PlayerInput & controller, bool multipleWalls)
{
	bool collision = false;

	if (DistanceAway(ship) < 7)
	{
		Vec2 a = GetPointA();
		Vec2 b = GetPointB();

		Vec2 c = ship.GetPosition();

		if (((c - a).Length() > (a - b).Length()) || ((c - b).Length() > (b - a).Length()))
			return false;

		Vec2 RelitiveA = (c - a).Normalize();
		Vec2 RelitiveB = (c - b).Normalize();
		Vec2 vol = ship.GetVolocity();

		Vec2 ProjectDirection = RelitiveA - RelitiveB;
		float MovingInto = vol.Normalize().Cross(ProjectDirection.Normalize() * 2);
		float cross = RelitiveA.Cross(RelitiveB);

		if (MovingInto < 0 && cross < 0) {
			collision = true;
		}
		if (MovingInto > 0 && cross > 0) {
			collision = true;
		}

		if (collision) 
		{
			if (multipleWalls)
			{ //After a bit of testing this seems to work the best
				controller.Wall(Vec2(-ship.GetVolocity() * 0.1));
			}
			Vec2 ProjectDirection = RelitiveA - RelitiveB;
			Vec2 Direction = ProjectDirection.Project(Vec2(vol.x, 0) + ProjectDirection.Project(Vec2(0, vol.y))) * ship.GetVolocity()
				* ProjectDirection.Project(Vec2(vol.x, 0) + ProjectDirection.Project(Vec2(0, vol.y)));
			
			controller.Wall(Direction);
		}
	}
	return collision;
}

float Wall::DistanceAway(GameObject & ship)
{
	Vec2 a = GetPointA();
	Vec2 b = GetPointB();

	Vec2 c = ship.GetPosition();

	float A = c.x - a.x;
	float B = c.y - a.y;
	float C = b.x - a.x;
	float D = b.y - a.y;

	float dot = A * C + B * D;
	float len_sq = C * C + D * D;
	float param = -1;

	if (len_sq != 0)
		param = dot / len_sq;

	float xx, yy;

	if (param < 0) {
		xx = a.x;
		yy = a.y;
	}
	else if (param > 1) {
		xx = b.x;
		yy = b.y;
	}
	else {
		xx = a.x + param * C;
		yy = a.y + param * D;
	}

	float dx = c.x - xx;
	float dy = c.y - yy;

	return sqrt(dx * dx + dy * dy);
}

Vec2 Wall::GetPointA() const
{
	return m_PointA;
}

Vec2 Wall::GetPointB() const
{
	return m_PointB;
}
